import { Routes, Route } from "react-router-dom";
import Login from "./pages/Login/Login";
import SignUp from "./pages/SignUp/SignUp";
import Home from "./pages/Home/Home";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Profile from "./pages/Profile/Profile";
import JobsContainer from "./pages/Jobs/JobsContainer";
import JobPage from "./pages/Jobs/JobPage/JobPage";
import CompanyList from "./pages/Company/CompanyList";
import Result from "./pages/Result/Result";
import Resume from "./pages/Resume/Resume";
import NotFound from "./pages/NotFound/NotFound";
import { useEffect } from "react";
import { getCompanyList } from "./pages/Company/CompanyApi";
import { useAppDispatch } from "./redux/hooks";
import { getJobsList } from "./pages/Jobs/JobsApi";
import ProfileViewCompany from "./pages/Profile/ProfileView/ProfileViewCompany";
import ProfileViewEmployee from "./pages/Profile/ProfileView/ProfileViewEmployee";

function App() {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getJobsList());
  }, [dispatch]);

  useEffect(() => {
    dispatch(getCompanyList());
  }, [dispatch]);

  return (
    <>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/login" element={<Login />} />
        <Route path="/signup" element={<SignUp />} />
        <Route path="/jobs" element={<JobsContainer />} />
        <Route path="/job-detail/:id" element={<JobPage />} />
        <Route path="/company-list" element={<CompanyList />} />
        <Route path="/info/:id" element={<Profile />} />
        <Route path="/result" element={<Result />} />
        <Route path="/create-resume/:id" element={<Resume />} />
        <Route path="/edit-resume/:id/:cvId" element={<Resume />} />
        <Route
          path="/profile-view-company/:id"
          element={<ProfileViewCompany />}
        />
        <Route
          path="/profile-view-employee/:id"
          element={<ProfileViewEmployee />}
        />
        <Route path="*" element={<NotFound />} />
      </Routes>
      <ToastContainer autoClose={1000} />
    </>
  );
}
export default App;
