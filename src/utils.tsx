export function formatDate(dateString: string) {
  const date = new Date(dateString);
  const month = date.getMonth();
  const day = date.getDate();
  const year = date.getFullYear();

  let suffix = "th";
  if (day === 1 || day === 21 || day === 31) {
    suffix = "st";
  } else if (day === 2 || day === 22) {
    suffix = "nd";
  } else if (day === 3 || day === 23) {
    suffix = "rd";
  }

  const monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  const formattedDate = `${monthNames[month]} ${day}${suffix} ${year}`;
  return formattedDate;
}

export const formatDateToMMYYYY = (dateString: string) => {
  return new Date(dateString).toLocaleString("en-US", {
    month: "2-digit",
    year: "numeric",
  });
};

export function formatTime(time: string) {
  const date = new Date(time);
  return date.toLocaleString();
}

export function formatDateToYYYYMMDD(dateString: string) {
  var pattern = /^\d{4}-\d{2}-\d{2}$/;
  if (pattern.test(dateString)) {
    return dateString; // Return the input string as it is
  }

  var date = new Date(dateString);
  var year = date.getFullYear();
  var month = ("0" + (date.getMonth() + 1)).slice(-2);
  var day = ("0" + date.getDate()).slice(-2);
  var formattedDate = year + "-" + month + "-" + day;
  return formattedDate;
}

export const convertMmToPixel = (mm: number) => {
  return (mm * 96) / 25.4;
};
