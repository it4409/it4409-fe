import React, { memo } from "react";
import { Link } from "react-router-dom";
const NotFound = () => {
  return (
    <div
      className="not-found container row"
      style={{
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)",
        fontSize: "1.5rem",
      }}
    >
      <div className="image-container col-8">
        <img src="/images/not-found.png" alt="Not Found" />
      </div>
      <div className="content-container col-4">
        <h1 style={{ color: "red", fontWeight: "900", fontSize: "8rem" }}>
          Oops!
        </h1>
        <p style={{ color: "black" }}>
          We're sorry, but the page you're looking for doesn't exist.
        </p>
        <p style={{ color: "black" }}>
          Or go back to <Link to="/">Home</Link> to discover many jobs for you!
        </p>
      </div>
    </div>
  );
};

export default memo(NotFound);
