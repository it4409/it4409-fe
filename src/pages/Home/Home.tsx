import React, { memo, useState } from "react";
import "./Home.scss";
import Navbar from "../../components/Navbar/Navbar";
import Footer from "../../components/Footer/Footer";
import { useNavigate } from "react-router-dom";
import { useAppDispatch } from "../../redux/hooks";
import { searchJob } from "../Jobs/JobsApi";
import { setSearchConditions } from "../Jobs/JobSlice";

const Home = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const [isScrolled, setIsScrolled] = useState(true);
  window.onscroll = () => {
    setIsScrolled(window.scrollY === 0 ? true : false);
    return () => (window.onscroll = null);
  };

  const [textSearch, settextSearch] = useState("");
  const [salarySearch, setSalarySearch] = useState<number>();
  const [locationSearch, setLocationSearch] = useState("");

  const handleSearch = async () => {
    await dispatch(
      searchJob({
        keyword: textSearch,
        locations: [locationSearch],
        salary: salarySearch,
      })
    );
    dispatch(
      setSearchConditions([
        textSearch,
        salarySearch ? `salary from ${salarySearch}$ ` : "",
        locationSearch ? `location : ${locationSearch}` : "",
      ])
    );
    navigate("/result");
  };

  return (
    <div className="home">
      <Navbar isScrolled={isScrolled} />
      <div className="search bg-dark">
        <div className="container">
          <h1 className="flex j-center text-light">Quality IT Job</h1>
          <form>
            <div className="form-group pb-2">
              <label className="text-light">
                Keyword skill, Jobs title, Company Name
              </label>
              <input
                type="text"
                className="form-control"
                placeholder="Keyword"
                value={textSearch}
                onChange={(e) => settextSearch(e.target.value)}
              />
            </div>
            <div className="form-group pb-2">
              <label className="text-light">Minimum salary</label>
              <input
                type="number"
                className="form-control"
                placeholder="Only number"
                value={salarySearch}
                onChange={(e) => setSalarySearch(Number(e.target.value))}
              />
            </div>
            <div className="form-group pb-2">
              <label className="text-light">Search by location</label>
              <input
                type="text"
                className="form-control"
                placeholder="Location"
                value={locationSearch}
                onChange={(e) => setLocationSearch(e.target.value)}
              />
            </div>
            <button
              type="button"
              className="btn btn-danger my-4 px-5"
              disabled={!textSearch && !salarySearch && !locationSearch}
              onClick={handleSearch}
            >
              Search
            </button>
          </form>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default memo(Home);
