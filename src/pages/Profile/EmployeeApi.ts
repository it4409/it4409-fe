import { createAsyncThunk } from "@reduxjs/toolkit";
import { BASE_URL } from "../../ultils/constant";
import {
  Application,
  Education,
  Resume,
  Skill,
  WorkingExperience,
} from "../../ultils/type";
import axiosWithAuth from "../../axiosInstance";
import { toast } from "react-toastify";

export const getEmployeeProfile = createAsyncThunk<any, { id: string }>(
  "/get-employee-info",
  async (payload) => {
    return await (
      await axiosWithAuth.get(`${BASE_URL}/employee-info?userId=${payload.id}`)
    ).data;
  }
);

export const editEmployeeProfile = createAsyncThunk<
  any,
  {
    name: string;
    avatar: string;
    email: string;
    gender: string;
    dateOfBirth: string;
    location: string;
    linkedInLink: string;
    gitHubLink: string;
  }
>("/edit-employee-info", async (payload) => {
  return await axiosWithAuth
    .put(`${BASE_URL}/employee-info`, {
      name: payload.name,
      avatar: payload.avatar,
      email: payload.email,
      gender: payload.gender,
      dateOfBirth: payload.dateOfBirth,
      location: payload.location,
      linkedInLink: payload.linkedInLink,
      gitHubLink: payload.gitHubLink,
    })
    .catch((err) => {
      toast.error(String(err));
    });
});

export const createEmployeeEdu = createAsyncThunk<
  any,
  {
    educations: [
      { schoolName: string; major: string; from?: string; to?: string }
    ];
  }
>("/create-employee-edu", async (payload) => {
  return await axiosWithAuth.post(`${BASE_URL}/employee-education`, {
    educations: [
      {
        schoolName: payload.educations[0].schoolName,
        major: payload.educations[0].major,
        from: payload.educations[0].from,
        to: payload.educations[0].to,
      },
    ],
  });
});

export const deleteEmployeeEdu = createAsyncThunk<any, { id: string }>(
  "/delete-employee-edu",
  async (payload) => {
    return await axiosWithAuth.delete(
      `${BASE_URL}/employee-education?educationId=${payload.id}`
    );
  }
);

export const createEmployeeWorkExperience = createAsyncThunk<
  any,
  {
    workingExperiences: [
      {
        position: string;
        companyName: string;
        responsibility: string;
        from: string;
        to: string;
      }
    ];
  }
>("/create-employee-working-experience", async (payload) => {
  return await axiosWithAuth.post(`${BASE_URL}/employee-working-experience`, {
    workingExperiences: [
      {
        position: payload.workingExperiences[0].position,
        companyName: payload.workingExperiences[0].companyName,
        responsibility: payload.workingExperiences[0].responsibility,
        from: payload.workingExperiences[0].from,
        to: payload.workingExperiences[0].to,
      },
    ],
  });
});

export const deleteEmployeeWorkExperience = createAsyncThunk<
  any,
  { id: string }
>("/delete-employee-working-experience", async (payload) => {
  return await axiosWithAuth.delete(
    `${BASE_URL}/employee-working-experience?workingExperienceId=${payload.id}`
  );
});

export const createEmployeeSkill = createAsyncThunk<
  any,
  {
    skills: [
      {
        title: string;
        certificateLink: string;
        score: string;
      }
    ];
  }
>("/create-employee-skill", async (payload) => {
  return await axiosWithAuth.post(`${BASE_URL}/employee-skill`, {
    skills: [
      {
        title: payload.skills[0].title,
        certificateLink: payload.skills[0].certificateLink,
        score: payload.skills[0].score,
      },
    ],
  });
});

export const deleteEmployeeSkill = createAsyncThunk<any, { id: string }>(
  "/delete-employee-skill",
  async (payload) => {
    return await axiosWithAuth.delete(
      `${BASE_URL}/employee-skill?skillId=${payload.id}`
    );
  }
);

export const getMyCV = createAsyncThunk<Resume[], { id: string }>(
  "get-resume",
  async (payload) => {
    return await (
      await axiosWithAuth.get(`${BASE_URL}/resume?employeeId=${payload.id}`)
    ).data;
  }
);
export const uploadEmployeeResume = createAsyncThunk<
  string,
  {
    employeeId: string;
    title: string;
    link: string;
    name: string;
    gender: string;
    dateOfBirth: string;
    location: string;
    linkedInLink: string;
    gitHubLink: string;
    email: string;
    mobilePhone: string;
    method: string;
  }
>("upload-resume", async (payload) => {
  return await axiosWithAuth.post(`${BASE_URL}/resume/create`, {
    employeeId: payload.employeeId,
    title: payload.title,
    link: payload.link,
    name: payload.name,
    gender: payload.gender,
    dateOfBirth: payload.dateOfBirth,
    location: payload.location,
    linkedInLink: payload.linkedInLink,
    gitHubLink: payload.gitHubLink,
    mobilePhone: payload.mobilePhone,
    email: payload.email,
    method: payload.method,
  });
});

export const uploadEmployeeResumePlus = createAsyncThunk<
  string,
  {
    title: string;
    link: string;
    email: string;
    gender: string;
    name: string;
    dateOfBirth: string;
    location: string;
    linkedInLink: string;
    gitHubLink: string;
    mobilePhone: string;
    skills: Skill[];
    workingExperiences: WorkingExperience[];
    educations: Education[];
    method: string;
  }
>("upload-resume", async (payload) => {
  return await axiosWithAuth.post(`${BASE_URL}/resume/create`, {
    title: payload.title,
    link: payload.link,
    mobilePhone: payload.mobilePhone,
    name: payload.name,
    gender: payload.gender,
    dateOfBirth: payload.dateOfBirth,
    location: payload.location,
    linkedInLink: payload.linkedInLink,
    gitHubLink: payload.gitHubLink,
    email: payload.email,
    method: payload.method,
    skills: payload.skills
      ? payload.skills.map((skill) => ({
        title: skill.title,
        certificateLink: skill.certificateLink,
        score: skill.score,
      }))
      : [],
    workingExperiences: payload.workingExperiences
      ? payload.workingExperiences.map((workExp) => ({
        position: workExp.position,
        companyName: workExp.companyName,
        responsibility: workExp.responsibility,
        from: workExp.from,
        to: workExp.to,
      }))
      : [],
    educations: payload.educations
      ? payload.educations.map((edu) => ({
        schoolName: edu.schoolName,
        major: edu.major,
        from: edu.from,
        to: edu.to,
      }))
      : [],
  });
});

export const deleteEmployeeResume = createAsyncThunk<any, { id: string }>(
  "delete-resume",
  async (payload) => {
    return await axiosWithAuth.delete(
      `${BASE_URL}/resume?resumeId=${payload.id}`
    );
  }
);

export const updateEmployeeResume = createAsyncThunk<
  any,
  {
    id: string;
    title: string;
    link: string;
    email: string;
    gender: string;
    name: string;
    dateOfBirth: string;
    location: string;
    linkedInLink: string;
    gitHubLink: string;
    mobilePhone: string;
    skills: Skill[];
    workingExperiences: WorkingExperience[];
    educations: Education[];
    method: string;
  }
>("update-resume", async (payload) => {
  return await axiosWithAuth.put(`${BASE_URL}/resume?resumeId=${payload.id}`, {
    title: payload.title,
    link: payload.link,
    mobilePhone: payload.mobilePhone,
    name: payload.name,
    gender: payload.gender,
    dateOfBirth: payload.dateOfBirth,
    location: payload.location,
    linkedInLink: payload.linkedInLink,
    gitHubLink: payload.gitHubLink,
    email: payload.email,
    method: payload.method,
    skills: payload.skills
      ? payload.skills.map((skill) => ({
        title: skill.title,
        certificateLink: skill.certificateLink,
        score: skill.score,
      }))
      : [],
    workingExperiences: payload.workingExperiences
      ? payload.workingExperiences.map((workExp) => ({
        position: workExp.position,
        companyName: workExp.companyName,
        responsibility: workExp.responsibility,
        from: workExp.from,
        to: workExp.to,
      }))
      : [],
    educations: payload.educations
      ? payload.educations.map((edu) => ({
        schoolName: edu.schoolName,
        major: edu.major,
        from: edu.from,
        to: edu.to,
      }))
      : [],
  });
});

interface IApplication {
  employeeId: string;
  jobPostId: string;
  resumeId: string;
  status: string;
  createdAt: string;
  updatedAt: string;
  id: string;
}

export const applyJob = createAsyncThunk<
  IApplication,
  {
    jobPostId: string;
    resumeId: string;
  }
>("apply-job", async (payload) => {
  return await axiosWithAuth.post(`${BASE_URL}/employee/application`, {
    jobPostId: payload.jobPostId,
    resumeId: payload.resumeId,
  });
});

export const getEmployeeApplication = createAsyncThunk<
  Application[],
  {
    employeeId: string;
  }
>("/get-employee-application-list", async (payload) => {
  const res = (
    await axiosWithAuth.get(`${BASE_URL}/employee/application`, {
      params: payload,
    })
  ).data;
  return res;
});

export const updateEducation = createAsyncThunk<any, Education>(
  "/update-employee-education",
  async (payload) => {
    return await axiosWithAuth.put(`${BASE_URL}/employee-education?educationId=${payload.id}`,
      {
        schoolName: payload.schoolName,
        major: payload.major,
        from: payload.from,
        to: payload.to
      });
  });

export const updateExperience = createAsyncThunk<any, WorkingExperience>(
  "/update-employee-experience",
  async (payload) => {
    return await axiosWithAuth.put(`${BASE_URL}/employee-working-experience?workingExperienceId=${payload.id}`,
      {
        positon: payload.position,
        companyName: payload.companyName,
        responsibility: payload.responsibility,
        from: payload.from,
        to: payload.to
      });
  });

export const updateSkill = createAsyncThunk<any, Skill>(
  "/update-employee-skill",
  async (payload) => {
    return await axiosWithAuth.put(`${BASE_URL}/employee-skill?skillId=${payload.id}`,
      {
        title: payload.title,
        certificateLink: payload.certificateLink,
        score: payload.score,
      });
  });
