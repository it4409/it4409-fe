import { useState, useEffect, useRef, useCallback, memo } from "react";
import { Button, Col, Form, Modal, Row } from "react-bootstrap";
import { useAppDispatch, useAppSelector } from "../../redux/hooks";
import { Company, Job } from "../../ultils/type";
import {
  changeStateJobPost,
  createEmployerJob,
  deleteEmployerJob,
  editEmployerJob,
  getApplicationList,
  getEmployerJob,
  replyApplication,
} from "./CompanyApi";
import {
  addJob,
  changeStateJob,
  deleteJob,
  setSelectedJob,
} from "./ProfileSlice";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";
import { handleFile } from "../../services/handleFile";
import ApplicationList from "../../components/Application/Application";

interface CompanyProfileProps {
  companyId: string;
  company: Company;
  handleChangeCompanyProfile: (company: Company) => void;
}

function CompanyProfile(props: CompanyProfileProps) {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const companyJobList = useAppSelector(
    (store) => store.profile.currentCompanyJob
  );
  const selectedJob = useAppSelector((store) => store.profile.selectedJob);
  const applicationList = useAppSelector(
    (store) => store.profile.applicationList
  );

  const inputRef = useRef<HTMLInputElement>(null);

  const [companyProfile, setCompanyProfile] = useState({} as Company);
  const [showContact, setShowContact] = useState(false);
  const [showEdit, setShowEdit] = useState(false);
  const [showJobList, setshowJobList] = useState(false);
  const [showAddJob, setShowAddJob] = useState(false);
  const [showApplication, setShowApplication] = useState(false);
  const [newJob, setNewJob] = useState({
    title: "",
    active: false,
    description: "",
    locations: [""],
    salary: 0,
    keywords: [
      {
        title: "",
        score: "",
      },
    ],
    requirement: "",
  });
  const [editJob, setEditJob] = useState({} as Job);

  const [showEditJob, setShowEditJob] = useState(false);

  const handleEditJob = (eidtJob: Job) => {
    dispatch(editEmployerJob(editJob))
      .then((res) => {
        if (res.payload) {
          toast.success("Edit job successfully");
        }
      })
      .catch((err) => {
        toast.error(String(err));
      });
    setShowEditJob(false);
  };

  const [locations, setLocations] = useState<string[]>([]);
  const [showAddLocation, setShowAddLocation] = useState(false);
  const [newLocation, setNewLocation] = useState("");

  useEffect(() => {
    setCompanyProfile(props.company);
    setLocations(props.company.locations);
  }, [props.company]);

  const handleSaveChanges = () => {
    props.handleChangeCompanyProfile(companyProfile);
    setShowEdit(false);
  };

  const getCompanyJobPost = async () => {
    await dispatch(getEmployerJob({ companyId: props.company.id }));
    setshowJobList(true);
  };

  const handleUploadLogo = async (e: any) => {
    const file = e.target.files[0];
    await dispatch(handleFile(file)).then((res) => {
      if (res.payload) {
        setCompanyProfile((prev) => {
          return {
            ...prev,
            logoLink: res.payload,
          };
        });
      }
    });
  };

  const handleAddJob = () => {
    setNewJob((prev) => {
      return {
        ...prev,
        locations: props.company.locations,
      };
    });
    dispatch(createEmployerJob(newJob))
      .then((res) => {
        if (res.payload) {
          dispatch(addJob(res.payload.data));
          toast.success("Create job successfully");
        }
      })
      .catch((err) => {
        toast.error(String(err));
      });
    setShowAddJob(false);
  };

  const handleEditStateJob = (job: Job) => {
    let isConfirm = window.confirm("Do you want to change state of the job");
    if (!isConfirm) {
      return;
    }
    dispatch(changeStateJobPost({ job: job }))
      .then((res) => {
        if (res.payload) {
          dispatch(changeStateJob(job.id));
        }
      })
      .catch((err) => {
        toast.error(String(err));
      });
  };

  const handleDeleteJob = (id: string) => {
    let isConfirm = window.confirm("Do you want to delete this job");
    if (!isConfirm) {
      return;
    }
    dispatch(deleteEmployerJob({ jobPostId: id }))
      .then((res) => {
        dispatch(deleteJob(res.meta.arg.jobPostId));
      })
      .catch((err) => {
        toast.error(String(err));
      });
  };

  const getJobApplicationList = async (job: Job) => {
    let res = await dispatch(getApplicationList({ jobId: job.id }));
    if (res.meta.requestStatus === "fulfilled") {
      dispatch(setSelectedJob(job));
      setShowApplication(true);
    }
  };

  const handleReply = useCallback(
    async (appId: string, replyStatus: string) => {
      let confirm = window.confirm(
        `Do you want to ${replyStatus.slice(
          0,
          replyStatus.length - 2
        )} this application`
      );
      if (!confirm) return;
      let res = await dispatch(
        replyApplication({ applicationId: appId, status: replyStatus })
      );
      if (res.payload) {
        toast.success("Successfully");
      }
    },
    [dispatch]
  );

  const filterApplication = useCallback(
    async (
      jobId: string,
      filter: {
        skillTitle?: string;
        yearOfWorkingExperience?: number;
        schoolName?: string;
      }
    ) => {
      await dispatch(getApplicationList({ jobId: jobId, ...filter }));
    },
    [dispatch]
  );
  return (
    <>
      <div
        className="bg-dark"
        style={{ minHeight: "500px", height: "fit-content", padding: "50px" }}
      >
        <div className="basic-info container">
          <div className="row">
            <div className="col-3 avatar">
              <img
                src={props.company.logoLink || ""}
                alt="avatar"
                style={{
                  width: "100%",
                  height: "100%",
                  borderRadius: "50%",
                }}
              />
            </div>
            <div className="col-6">
              <div className="row" style={{ margin: "30px 50px" }}>
                <div className="d-flex gap-5 align-items-center">
                  <div className=" name">{props.company.name}</div>
                  <Button
                    className="btn btn-md btn-primary"
                    onClick={() => setShowEdit(true)}
                  >
                    Edit
                  </Button>
                </div>
                {!!props.company.websiteLink && (
                  <div className="col-12 website">
                    {props.company.websiteLink}
                  </div>
                )}
                {!!props.company.locations &&
                  !!props.company.locations.length && (
                    <div className="col-12 locations">
                      Address:{" "}
                      {props.company.locations?.map((loc) => loc).join("| ")}
                    </div>
                  )}

                <Button
                  className="col-8 contact mt-5"
                  onClick={() => setShowContact(true)}
                >
                  Contact Information
                </Button>
              </div>
            </div>
          </div>
          <div className="row mt-2">
            <div className="col-12 description mb-5">{props.company.email}</div>
            {props.company.description && (
              <div className="col-12 description mb-5">
                <pre>{props.company.description}</pre>
              </div>
            )}
          </div>
          <button
            className="badge badge-pill bg-warning p-3"
            onClick={getCompanyJobPost}
            style={{ cursor: "pointer" }}
            disabled={showJobList}
          >
            {showJobList ? "" : "Get your job post"}
          </button>
          <hr />
          {showJobList && (
            <div className="container m-4">
              <h2>You had {companyJobList.length} job already</h2>
              <button
                className="btn btn-lg bg-primary"
                onClick={() => setShowAddJob(true)}
              >
                Add a new job
              </button>
              {companyJobList.length > 0 && (
                <table className="table" style={{ color: "white" }}>
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Job ID</th>
                      <th scope="col">Job Title</th>
                      <th scope="col">Job State</th>
                      <th scope="col"></th>
                      <th scope="col"></th>
                    </tr>
                  </thead>
                  <tbody>
                    {companyJobList.map((job, index: number) => (
                      <tr key={index}>
                        <th scope="row">{index}</th>
                        <td>{job.id}</td>
                        <td>{job.title}</td>
                        <td>
                          <span
                            className={`badge badge-pill ${
                              job.active ? "bg-success" : "bg-secondary"
                            }`}
                            onClick={() => handleEditStateJob(job)}
                            style={{ cursor: "pointer" }}
                          >
                            {job.active ? "active" : "inactive"}
                          </span>
                        </td>
                        <td>
                          <button
                            className="badge badge-pill bg-secondary m-1"
                            onClick={() => navigate(`/job-detail/${job.id}`)}
                          >
                            See Details
                          </button>
                          <button
                            className="badge badge-pill bg-warning m-1"
                            onClick={() => {
                              setShowEditJob(true);
                              setEditJob(job);
                            }}
                          >
                            Edit Job
                          </button>
                          <button
                            className="badge badge-pill bg-danger m-1"
                            onClick={() => handleDeleteJob(job.id)}
                          >
                            Delete
                          </button>
                        </td>
                        <td>
                          <button
                            className="badge badge-pill bg-primary m-1"
                            onClick={() => getJobApplicationList(job)}
                          >
                            See Applications
                          </button>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              )}
            </div>
          )}
        </div>
      </div>

      <Modal show={showEdit} onHide={() => setShowEdit(false)} size="lg">
        <Modal.Header closeButton>
          <Modal.Title>Edit company information</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ paddingLeft: "30px" }}>
          <Form.Label>Company name</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter company name"
            defaultValue={props.company.name}
            onChange={(e) =>
              setCompanyProfile((prev) => {
                return {
                  ...prev,
                  name: e.target.value,
                };
              })
            }
          />
          <Form.Label>Logo link</Form.Label>
          <Form.Control
            type="file"
            placeholder="Enter logo link"
            defaultValue={""}
            ref={inputRef}
            accept="image/png,image/jpeg,image/jpg,image/gif"
            onChange={(e) => handleUploadLogo(e)}
          />
          <Form.Label>Website link</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter website link"
            defaultValue={props.company.websiteLink}
            onChange={(e) =>
              setCompanyProfile((prev) => {
                return {
                  ...prev,
                  websiteLink: e.target.value,
                };
              })
            }
          />
          <Form.Label>Description</Form.Label>
          <Form.Control
            as="textarea"
            rows={5}
            placeholder="Enter description"
            defaultValue={props.company.description}
            onChange={(e) =>
              setCompanyProfile((prev) => {
                return {
                  ...prev,
                  description: e.target.value,
                };
              })
            }
          />
          <Form.Label>Locations</Form.Label>
          <Form>
            <div className="w-100">
              {locations &&
                locations.map((location, index) => {
                  return (
                    <div key={index} className=" p-1 btn-group btn-group-xs ">
                      <span className="btn btn-secondary">{location}</span>
                      <span
                        className="btn btn-remove btn-secondary"
                        onClick={() => {
                          let newLocation = locations.filter(
                            (lct) => lct !== location
                          );
                          setLocations(newLocation);
                          setCompanyProfile((prev) => {
                            return {
                              ...prev,
                              locations: newLocation,
                            };
                          });
                        }}
                      >
                        &times;
                      </span>
                    </div>
                  );
                })}
            </div>
            {showAddLocation && (
              <Form.Control
                placeholder="Enter location"
                className="w-25"
                value={newLocation}
                onChange={(e) => setNewLocation(e.target.value)}
              />
            )}
            <span
              className="small badge bg-primary"
              onClick={() => {
                if (showAddLocation) {
                  if (locations.includes(newLocation)) {
                    toast.error("Location already exists");
                  } else {
                    setLocations((prev) => {
                      return [...prev, newLocation];
                    });
                    setNewLocation("");
                    setCompanyProfile((prev) => {
                      return {
                        ...prev,
                        locations: [...locations, newLocation],
                      };
                    });
                  }
                } else {
                  setShowAddLocation(true);
                }
              }}
            >
              {showAddLocation ? "Add" : "Add new location"}
            </span>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowEdit(false)}>
            Close
          </Button>
          <Button variant="primary" onClick={() => handleSaveChanges()}>
            Save changes
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal show={showContact} onHide={() => setShowContact(false)}>
        <Modal.Header closeButton>
          <Modal.Title>{props.company.name}</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ paddingLeft: "30px" }}>
          <div className="row">Email: {props.company.email}</div>
          <div className="row">Mobile Phone: {props.company.mobilePhone}</div>
          <div className="row">Website: {props.company.websiteLink}</div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowContact(false)}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal show={showAddJob} onHide={() => setShowAddJob(false)} size="lg">
        <Modal.Header closeButton>
          <Modal.Title>Add a new job</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ paddingLeft: "30px" }}>
          <Form.Label>Title</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter Job Title"
            defaultValue={newJob.title}
            onChange={(e) =>
              setNewJob((prev) => {
                return {
                  ...prev,
                  title: e.target.value,
                };
              })
            }
          />
          <Form.Label>Description</Form.Label>
          <Form.Control
            type="text"
            as="textarea"
            rows={5}
            placeholder="Enter Job Description"
            defaultValue={newJob.description}
            onChange={(e) =>
              setNewJob((prev) => {
                return {
                  ...prev,
                  description: e.target.value,
                };
              })
            }
          />
          <Form.Label>Salary</Form.Label>
          <Form.Control
            type="numeric"
            placeholder="Enter salary"
            value={newJob.salary || ""}
            onChange={(e) => {
              if (isNaN(Number(e.target.value))) {
                toast.error("Salary must be number");
                setNewJob((prev) => {
                  return {
                    ...prev,
                    salary: 0,
                  };
                });
              } else if (typeof Number(e.target.value) === "number") {
                setNewJob((prev) => {
                  return {
                    ...prev,
                    salary: Number(e.target.value),
                  };
                });
              } else {
                toast.error("invalid salary");
              }
            }}
          />
          <Form.Label>Requirement</Form.Label>
          <Form.Control
            type="text"
            as="textarea"
            rows={5}
            placeholder="Enter requirement of the job"
            defaultValue={newJob.requirement}
            onChange={(e) =>
              setNewJob((prev) => {
                return {
                  ...prev,
                  requirement: e.target.value,
                };
              })
            }
          />
          <Form.Label>Keyword for job</Form.Label>
          <Form>
            {newJob.keywords.map((keyword, index) => {
              return (
                <Row key={index} className="mb-1">
                  <Col>
                    <Form.Control
                      placeholder="title"
                      value={keyword.title}
                      onChange={(e) =>
                        setNewJob((prev) => {
                          return {
                            ...prev,
                            keywords: prev.keywords.map((kw, idx) => {
                              if (idx === index) {
                                return {
                                  ...kw,
                                  title: e.target.value,
                                };
                              } else return kw;
                            }),
                          };
                        })
                      }
                    />
                  </Col>
                  <Col>
                    <Form.Control
                      placeholder="score"
                      value={keyword.score}
                      onChange={(e) =>
                        setNewJob((prev) => {
                          return {
                            ...prev,
                            keywords: prev.keywords.map((kw, idx) => {
                              if (idx === index) {
                                return {
                                  ...kw,
                                  score: e.target.value,
                                };
                              } else return kw;
                            }),
                          };
                        })
                      }
                    />
                  </Col>
                  <Col>
                    <Button
                      className="bg-danger border-danger bg-opacity-80 border-opacity-50"
                      onClick={() => {
                        let newKws = [...newJob.keywords];
                        newKws.splice(index, 1);
                        setNewJob((prev) => {
                          return {
                            ...prev,
                            keywords: newKws,
                          };
                        });
                      }}
                    >
                      &times;
                    </Button>
                  </Col>
                </Row>
              );
            })}
            <span
              className="small badge bg-primary"
              onClick={() =>
                setNewJob((prev) => {
                  return {
                    ...prev,
                    keywords: [
                      ...prev.keywords,
                      {
                        title: "",
                        score: "",
                      },
                    ],
                  };
                })
              }
            >
              Add new keywords
            </span>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowAddJob(false)}>
            Close
          </Button>
          <Button variant="primary" onClick={() => handleAddJob()}>
            Save changes
          </Button>
        </Modal.Footer>
      </Modal>

      {/* EDIT JOB MODAL */}
      <Modal show={showEditJob} onHide={() => setShowEditJob(false)} size="lg">
        <Modal.Header closeButton>
          <Modal.Title>Edit job</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ paddingLeft: "30px" }}>
          <Form.Label>Title</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter Job Title"
            defaultValue={editJob.title}
            onChange={(e) =>
              setEditJob((prev) => {
                return {
                  ...prev,
                  title: e.target.value,
                };
              })
            }
          />
          <Form.Label>Description</Form.Label>
          <Form.Control
            type="text"
            as="textarea"
            rows={6}
            placeholder="Enter Job Description"
            defaultValue={editJob.description}
            onChange={(e) =>
              setEditJob((prev) => {
                return {
                  ...prev,
                  description: e.target.value,
                };
              })
            }
          />
          <Form.Label>Salary</Form.Label>
          <Form.Control
            type="numeric"
            placeholder="Enter salary"
            value={editJob.salary || ""}
            onChange={(e) => {
              if (isNaN(Number(e.target.value))) {
                toast.error("Salary must be number");
                setEditJob((prev) => {
                  return {
                    ...prev,
                    salary: 0,
                  };
                });
              } else if (typeof Number(e.target.value) === "number") {
                setEditJob((prev) => {
                  return {
                    ...prev,
                    salary: Number(e.target.value),
                  };
                });
              } else {
                toast.error("invalid salary");
              }
            }}
          />
          <Form.Label>Requirement</Form.Label>
          <Form.Control
            type="text"
            as="textarea"
            rows={5}
            placeholder="Enter requirement of the job"
            defaultValue={editJob.requirement}
            onChange={(e) =>
              setEditJob((prev) => {
                return {
                  ...prev,
                  requirement: e.target.value,
                };
              })
            }
          />
          <Form.Label>Keyword for job</Form.Label>
          <Form>
            {editJob.keywords?.map((keyword, index) => {
              return (
                <Row key={index} className="mb-1">
                  <Col>
                    <Form.Control
                      placeholder="title"
                      value={keyword.title}
                      onChange={(e) =>
                        setEditJob((prev) => {
                          return {
                            ...prev,
                            keywords: prev.keywords?.map((kw, idx) => {
                              if (idx === index) {
                                return {
                                  ...kw,
                                  title: e.target.value,
                                };
                              } else return kw;
                            }),
                          };
                        })
                      }
                    />
                  </Col>
                  <Col>
                    <Form.Control
                      placeholder="score"
                      value={keyword.score}
                      onChange={(e) =>
                        setEditJob((prev) => {
                          return {
                            ...prev,
                            keywords: prev.keywords.map((kw, idx) => {
                              if (idx === index) {
                                return {
                                  ...kw,
                                  score: e.target.value,
                                };
                              } else return kw;
                            }),
                          };
                        })
                      }
                    />
                  </Col>
                  <Col>
                    <Button
                      className="bg-danger border-danger bg-opacity-80 border-opacity-50"
                      onClick={() => {
                        let newKws = [...editJob.keywords];
                        newKws.splice(index, 1);
                        setEditJob((prev) => {
                          return {
                            ...prev,
                            keywords: newKws,
                          };
                        });
                      }}
                    >
                      &times;
                    </Button>
                  </Col>
                </Row>
              );
            })}
            <span
              className="small badge bg-primary"
              onClick={() =>
                setEditJob((prev) => {
                  return {
                    ...prev,
                    keywords: [
                      ...prev.keywords,
                      {
                        id: "",
                        title: "",
                        score: "",
                      },
                    ],
                  };
                })
              }
            >
              Add new keywords
            </span>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowEditJob(false)}>
            Close
          </Button>
          <Button variant="primary" onClick={() => handleEditJob(editJob)}>
            Save changes
          </Button>
        </Modal.Footer>
      </Modal>

      {selectedJob && applicationList && (
        <ApplicationList
          show={showApplication}
          onHide={() => setShowApplication(false)}
          applicationList={applicationList}
          jobPostId={selectedJob.id}
          modalTitle={`Apply for ${selectedJob.title}`}
          isOwner={true}
          handleReply={handleReply}
          filterApplication={filterApplication}
        />
      )}
    </>
  );
}

export default memo(CompanyProfile);
