import { createAsyncThunk } from "@reduxjs/toolkit";
import { BASE_URL } from "../../ultils/constant";
import axiosWithAuth from "../../axiosInstance";
import { Application, Job } from "../../ultils/type";

export const getEmployerProfile = createAsyncThunk<any, { id: string }>(
  "/get-employer-info",
  async (payload) => {
    return await (
      await axiosWithAuth.get(`${BASE_URL}/employer-info?userId=${payload.id}`)
    ).data;
  }
);

export const editEmployerProfile = createAsyncThunk<
  any,
  {
    name: string;
    description: string;
    logoLink: string;
    websiteLink: string;
    locations: string[];
  }
>("/edit-employer-info", async (payload) => {
  await axiosWithAuth.put(`${BASE_URL}/employer-info`, {
    name: payload.name,
    description: payload.description,
    logoLink: payload.logoLink,
    websiteLink: payload.websiteLink,
    locations: payload.locations,
  });
  return payload;
});

export const getEmployerJob = createAsyncThunk<
  Job[],
  {
    companyId: string;
  }
>("/get-employer-job", async (payload) => {
  return (
    await axiosWithAuth.get(`${BASE_URL}/job?companyId=${payload.companyId}`)
  ).data;
});

export const createEmployerJob = createAsyncThunk<
  any,
  {
    title: string;
    active: boolean;
    description: string;
    locations: string[];
    salary: number;
    keywords: {
      title: string;
      score: string;
    }[];
    requirement: string;
  }
>("/create-employer-job", async (payload) => {
  return await axiosWithAuth.post(`${BASE_URL}/job`, {
    title: payload.title,
    active: payload.active,
    description: payload.description,
    locations: payload.locations,
    salary: payload.salary,
    keywords: payload.keywords,
    requirement: payload.requirement,
  });
});

export const editEmployerJob = createAsyncThunk<any, Job>(
  "/edit-employer-job",
  async (payload) => {
    return await axiosWithAuth.put(`${BASE_URL}/job?jobPostId=${payload.id}`, {
      title: payload.title,
      active: payload.active,
      description: payload.description,
      locations: payload.locations,
      salary: payload.salary,
      keywords: payload.keywords,
      requirement: payload.requirement,
    });
  }
);

export const changeStateJobPost = createAsyncThunk<any, { job: Job }>(
  "/change-state-employer-job",
  async (payload) => {
    return await axiosWithAuth.put(
      `${BASE_URL}/job?jobPostId=${payload.job.id}`,
      {
        active: !payload.job.active,
      }
    );
  }
);
export const deleteEmployerJob = createAsyncThunk<any, { jobPostId: string }>(
  "/delete-employer-job",
  async (payload) => {
    return await axiosWithAuth.delete(
      `${BASE_URL}/job?jobPostId=${payload.jobPostId}`
    );
  }
);

export const getApplicationList = createAsyncThunk<
  Application[],
  {
    jobId: string;
    skillTitle?: string;
    yearOfWorkingExperience?: number;
    schoolName?: string;
  }
>("/get-job-application-list", async (payload) => {
  const res = (
    await axiosWithAuth.get(`${BASE_URL}/employer/application`, {
      params: payload,
    })
  ).data;
  return res;
});
export const replyApplication = createAsyncThunk<
  any,
  { status: string; applicationId: string }
>("/reply-job-application", async (payload) => {
  return await axiosWithAuth.put(`${BASE_URL}/employer/application/reply`, {
    applicationId: payload.applicationId,
    status: payload.status,
  });
});
