import { memo, useCallback, useEffect, useRef, useState } from "react";
import "./EmployeeProfile.scss";
import {
  formatDate,
  formatDateToMMYYYY,
  formatTime,
  formatDateToYYYYMMDD,
} from "../../utils";
import { Button, Form, Modal } from "react-bootstrap";
import {
  Education,
  Employee,
  Skill,
  WorkingExperience,
} from "../../ultils/type";
import { useAppDispatch, useAppSelector } from "../../redux/hooks";
import { toast } from "react-toastify";
import {
  createEmployeeEdu,
  createEmployeeSkill,
  createEmployeeWorkExperience,
  deleteEmployeeEdu,
  deleteEmployeeResume,
  deleteEmployeeSkill,
  deleteEmployeeWorkExperience,
  getEmployeeApplication,
  getMyCV,
  updateEducation,
  updateExperience,
  updateSkill,
  uploadEmployeeResume,
} from "./EmployeeApi";
import {
  addEdu,
  addExperience,
  addSkill,
  deleteEdu,
  deleteExperience,
  deleteSkill,
  editEdu,
  editExperience,
  editSkill,
} from "./ProfileSlice";
import { useNavigate } from "react-router-dom";
import { handleFile } from "../../services/handleFile";
import Application from "../../components/Application/Application";

interface EmployeeProfileProps {
  employeeId: string;
  employee: Employee;
  handleChangeEmployeeProfile: (employee: Employee) => void;
}

const EmployeeProfile = memo((props: EmployeeProfileProps) => {
  const { employeeId, employee, handleChangeEmployeeProfile } = props;
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const InputCVRef = useRef<HTMLInputElement | null>(null);
  const [file, setFile] = useState<File | null>(null);
  const [resumeTitle, setResumeTitle] = useState("");
  const myCVList = useAppSelector((store) => store.profile.myCV);
  const myApplicationList = useAppSelector(
    (store) => store.profile.employeeApplyList
  );

  const [employeeProfile, setEmployeeProfile] = useState({} as Employee);
  const [newEducation, setNewEducation] = useState({
    schoolName: "",
    major: "",
    from: "",
    to: "",
  });
  const [newExperience, setNewExperience] = useState({
    position: "",
    companyName: "",
    responsibility: "",
    from: "",
    to: "",
  });
  const [newSkill, setNewSkill] = useState({
    title: "",
    certificateLink: "",
    score: "",
  });
  const [editingEdu, setEditingEdu] = useState<Education>({
    schoolName: "",
    major: "",
    from: "",
    to: "",
    id: "",
  });
  const [editingExperience, setEditingExperience] = useState<WorkingExperience>(
    {
      position: "",
      companyName: "",
      responsibility: "",
      from: "",
      to: "",
      id: "",
    }
  );
  const [editingSkill, setEditingSkill] = useState<Skill>({
    title: "",
    certificateLink: "",
    score: "",
    id: "",
  });

  const [showMyCV, setShowMyCV] = useState(false);
  const [showContact, setShowContact] = useState(false);
  const [showEditInfo, setShowEditInfo] = useState(false);
  const [showAddEducation, setShowAddEducation] = useState(false);
  const [showAddExperience, setShowAddExperience] = useState(false);
  const [showAddSkill, setShowAddSkill] = useState(false);
  const [showEditEdu, setShowEditEdu] = useState(false);
  const [showEditExperience, setShowEditExperience] = useState(false);
  const [showEditSkill, setShowEditSkill] = useState(false);
  const [showResumeTitle, setShowResumeTitle] = useState(false);
  const [showApplication, setShowApplication] = useState(false);

  useEffect(() => {
    setEmployeeProfile(employee);
  }, [employee]);

  const handleSaveChange = useCallback(() => {
    handleChangeEmployeeProfile(employeeProfile);
    setShowEditInfo(false);
  }, [employeeProfile, handleChangeEmployeeProfile]);

  const handleAddEducation = useCallback(() => {
    if (
      newEducation.schoolName !== "" &&
      newEducation.major !== "" &&
      newEducation.from !== "" &&
      newEducation.to !== ""
    ) {
      dispatch(
        createEmployeeEdu({
          educations: [
            {
              schoolName: newEducation.schoolName,
              major: newEducation.major,
              from: newEducation.from,
              to: newEducation.to,
            },
          ],
        })
      )
        .then((res) => {
          dispatch(addEdu(res.payload.data));
        })
        .catch((err) => {
          toast.error(String(err));
        });
      setShowAddEducation(false);
    } else toast.error("All field must be filled");
  }, [
    dispatch,
    newEducation.from,
    newEducation.major,
    newEducation.schoolName,
    newEducation.to,
  ]);

  const handleDeleteEdu = useCallback(
    (edu: Education) => {
      let confirm = window.confirm("Do you want to delete this information");
      if (!confirm) {
        return;
      }
      dispatch(deleteEmployeeEdu({ id: edu.id }))
        .then((res) => {
          if (res.payload) {
            dispatch(deleteEdu(res.meta.arg.id));
          }
        })
        .catch((err) => {
          toast.error(String(err));
        });
    },
    [dispatch]
  );

  const handleAddExperience = useCallback(() => {
    if (
      newExperience.companyName !== "" &&
      newExperience.position !== "" &&
      newExperience.from !== "" &&
      newExperience.to !== "" &&
      newExperience.responsibility !== ""
    ) {
      dispatch(
        createEmployeeWorkExperience({
          workingExperiences: [
            {
              companyName: newExperience.companyName,
              position: newExperience.position,
              responsibility: newExperience.responsibility,
              from: newExperience.from,
              to: newExperience.to,
            },
          ],
        })
      )
        .then((res) => {
          dispatch(addExperience(res.payload.data));
        })
        .catch((err) => {
          toast.error(String(err));
        });
      setShowAddExperience(false);
    } else toast.error("All field must be filled");
  }, [
    dispatch,
    newExperience.companyName,
    newExperience.from,
    newExperience.position,
    newExperience.responsibility,
    newExperience.to,
  ]);

  const handleDeleteExperience = useCallback(
    (exper: WorkingExperience) => {
      let confirm = window.confirm("Do you want to delete this information");
      if (!confirm) {
        return;
      }
      dispatch(deleteEmployeeWorkExperience({ id: exper.id })).then((res) => {
        if (res.payload) {
          dispatch(deleteExperience(res.meta.arg.id));
        }
      });
    },
    [dispatch]
  );

  const handleAddSkill = useCallback(() => {
    if (newSkill.title !== "") {
      dispatch(
        createEmployeeSkill({
          skills: [
            {
              title: newSkill.title,
              score: newSkill.score,
              certificateLink: newSkill.certificateLink,
            },
          ],
        })
      )
        .then((res) => {
          dispatch(addSkill(res.payload.data));
        })
        .catch((err) => {
          toast.error(String(err));
        });
      setShowAddSkill(false);
    } else toast.error("Skill title must be filled");
  }, [dispatch, newSkill.certificateLink, newSkill.score, newSkill.title]);

  const handleDeleteSkill = useCallback(
    (skill: Skill) => {
      let confirm = window.confirm("Do you want to delete this information");
      if (!confirm) {
        return;
      }
      dispatch(deleteEmployeeSkill({ id: skill.id })).then((res) => {
        if (res.payload) {
          dispatch(deleteSkill(res.meta.arg.id));
        }
      });
    },
    [dispatch]
  );

  const handleGetCV = useCallback(
    async (id: string) => {
      await dispatch(getMyCV({ id: id }));
      setShowMyCV(true);
    },
    [dispatch]
  );

  const handleUploadLogo = async (e: any) => {
    const file = e.target.files[0];
    await dispatch(handleFile(file)).then((res) => {
      if (res.payload) {
        setEmployeeProfile((prev) => {
          return {
            ...prev,
            avatar: res.payload,
          };
        });
      }
    });
  };

  const handleUploadCV = async (e: any) => {
    // const loading = toast.loading("Waiting a moment to upload your resume");
    // const file = e.target.files[0];
    // await dispatch(handleFile(file))
    //   .then((res) => {
    //     setUploadResume((prev) => ({ title: prev.title, link: res.payload }));
    //   })
    //   .catch((err) => {
    //     toast.error(String(err));
    //   })
    //   .finally(() => {
    //     toast.dismiss(loading);
    //   });
    setFile(e.target.files[0]);
  };

  const handleUploadResume = async () => {
    if (file === null) {
      toast.error("You haven't upload any resume");
      return;
    } else if (resumeTitle === "") {
      toast.error("You haven't set any title for uploaded resume");
      return;
    } else {
      const loading = toast.loading("Waiting a moment to upload your resume");
      await dispatch(handleFile(file)).then((res) => {
        if (res.payload) {
          dispatch(
            uploadEmployeeResume({
              employeeId: employee.id,
              title: resumeTitle,
              link: res.payload,
              name: employee.name,
              gender: employee.gender,
              dateOfBirth: employee.dateOfBirth,
              location: employee.location,
              linkedInLink: employee.linkedInLink,
              gitHubLink: employee.gitHubLink,
              email: employee.email,
              method: "upload",
              mobilePhone: employee.mobilePhone.startsWith("+84")
                ? employee.mobilePhone
                : employee.mobilePhone.startsWith("0")
                ? `+84${employee.mobilePhone.slice(1)}`
                : "",
            })
          )
            .then((resObj) => {
              toast.success("Upload resume successfully");
              setShowResumeTitle(false);
              setShowMyCV(false);
            })
            .catch((err) => {
              toast.error(String(err));
              setShowResumeTitle(false);
              setShowMyCV(false);
            })
            .finally(() => {
              // setUploadResume({ title: "", link: "" });
              setFile(null);
              setResumeTitle("");
              toast.dismiss(loading);
            });
        }
      });
    }
  };

  const handleDeleteResume = async (id: string) => {
    dispatch(deleteEmployeeResume({ id: id }))
      .then((r) => {
        toast.success("delete resume successfully");
      })
      .catch((err) => {
        toast.error(String(err));
      });
  };

  const handleEditResume = async (cvId: string) => {
    navigate(`/edit-resume/${employeeId}/${cvId}`);
  };

  const getMyApplication = async () => {
    let loading = toast.loading("Wait a minute to load...");
    await dispatch(getEmployeeApplication({ employeeId: employee.id }));
    toast.dismiss(loading);
    setShowApplication(true);
  };
  const handleEditEducation = async () => {
    let res = await dispatch(
      updateEducation({
        id: editingEdu.id,
        schoolName: editingEdu.schoolName,
        major: editingEdu.major,
        from: editingEdu.from,
        to: editingEdu.to,
      })
    );
    if (res) {
      dispatch(editEdu(editingEdu));
      toast.success("Successfully");
      setShowEditEdu(false);
    }
  };

  const handleEditExperience = async () => {
    let res = await dispatch(
      updateExperience({
        id: editingExperience.id,
        position: editingExperience.position,
        companyName: editingExperience.companyName,
        responsibility: editingExperience.responsibility,
        from: editingExperience.from,
        to: editingExperience.to,
      })
    );
    if (res) {
      dispatch(editExperience(editingExperience));
      toast.success("Successfully");
      setShowEditExperience(false);
    }
  };
  const handleEditSkill = async () => {
    let res = await dispatch(
      updateSkill({
        id: editingSkill.id,
        title: editingSkill.title,
        score: editingSkill.score,
        certificateLink: editingSkill.certificateLink,
      })
    );
    if (res) {
      dispatch(editSkill(editingSkill));
      toast.success("Successfully");
      setShowEditSkill(false);
    }
  };

  return (
    <>
      <div
        className="bg-dark"
        style={{ minHeight: "500px", height: "fit-content", padding: "50px" }}
      >
        <div className="basic-info container">
          <div className="row">
            <div className="col-3 avatar">
              {employee && employee.avatar && (
                <img
                  src={employee.avatar || ""}
                  alt="avatar"
                  style={{
                    width: "100%",
                    height: "100%",
                    borderRadius: "50%",
                  }}
                />
              )}
            </div>
            <div className="col-9">
              <div className="row" style={{ margin: "30px 50px" }}>
                <div className="d-flex gap-4 align-items-center">
                  <div className="name">{employee.name}</div>
                  <Button
                    className="btn btn-md btn-primary"
                    onClick={() => setShowEditInfo(true)}
                  >
                    Edit
                  </Button>
                </div>

                {employee.gender && (
                  <div className="col-12 gender">{employee.gender}</div>
                )}
                {employee.dateOfBirth && (
                  <div className="col-12 birth-day">
                    {formatDate(employee.dateOfBirth)}
                  </div>
                )}
                {employee.location && (
                  <div className="col-12 location">
                    Address: {employee.location}
                  </div>
                )}
                <div className="mt-5">
                  <Button
                    className="col-4 contact "
                    onClick={() => setShowContact(true)}
                  >
                    My Contact Information
                  </Button>
                  <Button
                    className="col-3 btn btn-warning ms-2"
                    onClick={() => handleGetCV(employeeId)}
                  >
                    My CV
                  </Button>
                  <Button
                    className="col-3 btn btn-danger ms-2"
                    onClick={getMyApplication}
                  >
                    See Job Applied
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <hr style={{ color: "white" }}></hr>

        <div className="container education ">
          {/* Show education */}
          <div className="d-flex gap-5">
            <div className="title ">Educations</div>
            <Button
              variant="primary"
              style={{ height: "fit-content" }}
              onClick={() => setShowAddEducation(true)}
            >
              Add
            </Button>
          </div>

          {!!employee.education && !!employee.education.length ? (
            employee.education.map((edu, index) => (
              <div className="row" key={index}>
                <div className="col-2" style={{ borderLeft: "1px solid #fff" }}>
                  <div className="row">
                    <div className="col-12 time text-center">
                      {edu.from && formatDateToMMYYYY(edu.from)} -
                      {edu.to && formatDateToMMYYYY(edu.to)}
                    </div>
                  </div>
                </div>
                <div className="col-9" style={{ borderLeft: "1px solid #fff" }}>
                  <div className="row">
                    <div className="col-12 school-name">{edu.schoolName}</div>
                    <div className="col-12 major">{edu.major}</div>
                  </div>
                </div>
                <div className="col-1">
                  <div className="row">
                    <Button
                      variant="warning"
                      style={{ height: "fit-content", marginBottom: "10px" }}
                      onClick={() => {
                        setEditingEdu(edu);
                        setShowEditEdu(true);
                      }}
                    >
                      Edit
                    </Button>
                    <Button
                      variant="danger"
                      style={{ height: "fit-content", marginBottom: "10px" }}
                      onClick={() => handleDeleteEdu(edu)}
                    >
                      Delete
                    </Button>
                  </div>
                </div>
                <hr />
              </div>
            ))
          ) : (
            <div className="d-flex justify-content-center text-center w-100 mt-4">
              <h4 style={{ fontWeight: "600", fontSize: "20px" }}>
                No education information was found. Please add one to update
                your profile ...
              </h4>
            </div>
          )}
        </div>

        <hr style={{ color: "white" }}></hr>
        <div className="container working">
          {/* Show working */}{" "}
          <div className="d-flex gap-5">
            <div className="title ">Working Experiences</div>
            <Button
              variant="primary"
              style={{ height: "fit-content" }}
              onClick={() => setShowAddExperience(true)}
            >
              Add
            </Button>
          </div>
          {!!employee.workingExperience && employee.workingExperience.length ? (
            employee.workingExperience?.map((work, index) => (
              <div className="row" key={index}>
                <div className="col-2" style={{ borderLeft: "1px solid #fff" }}>
                  <div className="row">
                    <div className="col-12 time text-center">
                      {work.from && formatDateToMMYYYY(work.from)} -{" "}
                      {work.to && formatDateToMMYYYY(work.to)}
                    </div>
                  </div>
                </div>
                <div className="col-9" style={{ borderLeft: "1px solid #fff" }}>
                  <div className="row">
                    <div className="col-12 company-name">
                      {work.companyName}
                    </div>
                    <div className="col-12 position">{work.position}</div>
                    <div className="col-12 responsibility">
                      <pre>{work.responsibility}</pre>
                    </div>
                  </div>
                </div>
                <div className="col-1 d-flex align-items-center">
                  <div className="row">
                    <Button
                      variant="warning"
                      style={{ height: "fit-content", marginBottom: "10px" }}
                      onClick={() => {
                        setEditingExperience(work);
                        setShowEditExperience(true);
                      }}
                    >
                      Edit
                    </Button>
                    <Button
                      variant="danger"
                      style={{ height: "fit-content" }}
                      onClick={() => handleDeleteExperience(work)}
                    >
                      Delete
                    </Button>
                  </div>
                </div>
                <hr />
              </div>
            ))
          ) : (
            <div className="d-flex justify-content-center text-center w-100 mt-4">
              <h4 style={{ fontWeight: "600", fontSize: "20px" }}>
                No working experience information was found. Please add one to
                update your profile ...
              </h4>
            </div>
          )}
        </div>

        <hr style={{ color: "white" }}></hr>
        <div className="container skill">
          {/* Show skill */}
          <div className="d-flex gap-5">
            <div className="title ">Skills</div>
            <Button
              variant="primary"
              style={{ height: "fit-content" }}
              onClick={() => setShowAddSkill(true)}
            >
              Add
            </Button>
          </div>
          {!!employee.skill && employee.skill.length ? (
            employee.skill?.map((skill, index) => (
              <div className="row d-flex justify-content-between" key={index}>
                {!!skill.score && (
                  <div
                    className="col-2"
                    style={{ borderLeft: "1px solid #fff" }}
                  >
                    <div className="row">
                      <div className="col-12 score">SCORE: {skill.score}</div>
                    </div>
                  </div>
                )}
                <div className="col-7" style={{ borderLeft: "1px solid #fff" }}>
                  <div className="row">
                    <div className="col-12 skill-name">{skill.title}</div>
                    {!!skill.certificateLink && (
                      <div className="col-12 certificateLink">
                        Certificate-link:
                        {"  " + skill?.certificateLink}
                      </div>
                    )}
                  </div>
                </div>
                <div className="col-1 mb-2">
                  <div className="row">
                    <Button
                      variant="warning"
                      style={{ height: "fit-content", marginBottom: "10px" }}
                      onClick={() => {
                        setEditingSkill(skill);
                        setShowEditSkill(true);
                      }}
                    >
                      Edit
                    </Button>
                    <Button
                      variant="danger"
                      style={{ height: "fit-content" }}
                      onClick={() => handleDeleteSkill(skill)}
                    >
                      Delete
                    </Button>
                  </div>
                </div>

                <hr />
              </div>
            ))
          ) : (
            <div className="d-flex justify-content-center text-center w-100 mt-4">
              <h4 style={{ fontWeight: "600", fontSize: "20px" }}>
                No skill information was found. Please add one to update your
                profile ...
              </h4>
            </div>
          )}
        </div>
      </div>

      <Modal
        show={showEditInfo}
        onHide={() => setShowEditInfo(false)}
        size="lg"
      >
        <Modal.Header closeButton>
          <Modal.Title>Edit Infomation</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ paddingLeft: "30px" }}>
          <Form.Label>Full name</Form.Label>
          <Form.Control
            placeholder="Enter name"
            defaultValue={employee.name}
            onChange={(e) =>
              setEmployeeProfile((prev) => {
                return {
                  ...prev,
                  name: e.target.value,
                };
              })
            }
          />
          <Form.Label>Avatar</Form.Label>
          <Form.Control
            type="file"
            placeholder="Upload avatar"
            defaultValue={""}
            accept="image/png,image/jpeg,image/jpg,image/gif"
            onChange={(e) => handleUploadLogo(e)}
          />
          <Form.Label>Gender</Form.Label>
          <Form.Control
            as="select"
            defaultValue={employee.gender}
            onChange={(e) =>
              setEmployeeProfile((prev) => {
                return {
                  ...prev,
                  gender: e.target.value,
                };
              })
            }
          >
            <option value="male">Male</option>
            <option value="female">Female</option>
            <option value="other">Other</option>
          </Form.Control>
          <Form.Label>Date of birth</Form.Label>
          <Form.Control
            defaultValue={employee.dateOfBirth}
            type="date"
            onChange={(e) =>
              setEmployeeProfile((prev) => {
                return {
                  ...prev,
                  dateOfBirth: e.target.value,
                };
              })
            }
          />
          <Form.Label>Location</Form.Label>
          <Form.Control
            defaultValue={employee.location}
            onChange={(e) =>
              setEmployeeProfile((prev) => {
                return {
                  ...prev,
                  location: e.target.value,
                };
              })
            }
          />
          <Form.Label>LinkedIn link</Form.Label>
          <Form.Control
            type="url"
            defaultValue={employee.linkedInLink}
            onChange={(e) =>
              setEmployeeProfile((prev) => {
                return {
                  ...prev,
                  linkedInLink: e.target.value,
                };
              })
            }
          />
          <Form.Label>GitHub link</Form.Label>
          <Form.Control
            type="url"
            defaultValue={employee.gitHubLink}
            onChange={(e) =>
              setEmployeeProfile((prev) => {
                return {
                  ...prev,
                  gitHubLink: e.target.value,
                };
              })
            }
          />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowEditInfo(false)}>
            Close
          </Button>
          <Button variant="primary" onClick={() => handleSaveChange()}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal show={showContact} onHide={() => setShowContact(false)}>
        <Modal.Header closeButton>
          <Modal.Title>{employee.name}</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ paddingLeft: "30px" }}>
          <div className="row">Email: {employee.email}</div>
          <div className="row">Mobile Phone: {employee.mobilePhone}</div>
          <div className="row">Linkedin: {employee.linkedInLink}</div>
          <div className="row">Github: {employee.gitHubLink}</div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowContact(false)}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal show={showAddEducation} onHide={() => setShowAddEducation(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Add new education</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ paddingLeft: "30px" }}>
          <Form.Label>School name</Form.Label>
          <Form.Control
            value={newEducation.schoolName}
            onChange={(e) =>
              setNewEducation((prev) => {
                return {
                  ...prev,
                  schoolName: e.target.value,
                };
              })
            }
          />
          <Form.Label>Major</Form.Label>
          <Form.Control
            value={newEducation.major}
            onChange={(e) =>
              setNewEducation((prev) => {
                return {
                  ...prev,
                  major: e.target.value,
                };
              })
            }
          />
          <Form.Label>From</Form.Label>
          <Form.Control
            type="date"
            value={newEducation.from}
            onChange={(e) =>
              setNewEducation((prev) => {
                return {
                  ...prev,
                  from: e.target.value,
                };
              })
            }
          />
          <Form.Label>To</Form.Label>
          <Form.Control
            type="date"
            value={newEducation.to}
            onChange={(e) =>
              setNewEducation((prev) => {
                return {
                  ...prev,
                  to: e.target.value,
                };
              })
            }
          />
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={() => setShowAddEducation(false)}
          >
            Close
          </Button>
          <Button variant="primary" onClick={handleAddEducation}>
            Add new
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal
        show={showEditEdu && !!editingEdu}
        onHide={() => setShowEditEdu(false)}
      >
        <Modal.Header closeButton>
          <Modal.Title>Edit your education</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ paddingLeft: "30px" }}>
          <Form.Label>School name</Form.Label>
          <Form.Control
            value={editingEdu.schoolName}
            onChange={(e) =>
              setEditingEdu((prev) => {
                return {
                  ...prev,
                  schoolName: e.target.value,
                };
              })
            }
          />
          <Form.Label>Major</Form.Label>
          <Form.Control
            value={editingEdu.major}
            onChange={(e) =>
              setEditingEdu((prev) => {
                return {
                  ...prev,
                  major: e.target.value,
                };
              })
            }
          />
          <Form.Label>From</Form.Label>
          <Form.Control
            type="date"
            value={editingEdu.from && formatDateToYYYYMMDD(editingEdu.from)}
            onChange={(e) =>
              setEditingEdu((prev) => {
                return {
                  ...prev,
                  from: e.target.value,
                };
              })
            }
          />
          <Form.Label>To</Form.Label>
          <Form.Control
            type="date"
            value={editingEdu.to && formatDateToYYYYMMDD(editingEdu.to)}
            onChange={(e) =>
              setEditingEdu((prev) => {
                return {
                  ...prev,
                  to: e.target.value,
                };
              })
            }
          />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowEditEdu(false)}>
            Cancel
          </Button>
          <Button variant="primary" onClick={handleEditEducation}>
            Save
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal
        show={showAddExperience}
        onHide={() => setShowAddExperience(false)}
      >
        <Modal.Header closeButton>
          <Modal.Title>Add new working experience</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ paddingLeft: "30px" }}>
          <Form.Label>Company name</Form.Label>
          <Form.Control
            value={newExperience.companyName}
            onChange={(e) =>
              setNewExperience((prev) => {
                return {
                  ...prev,
                  companyName: e.target.value,
                };
              })
            }
          />
          <Form.Label>Position</Form.Label>
          <Form.Control
            value={newExperience.position}
            onChange={(e) =>
              setNewExperience((prev) => {
                return {
                  ...prev,
                  position: e.target.value,
                };
              })
            }
          />
          <Form.Label>Responsibility</Form.Label>
          <Form.Control
            as="textarea"
            rows={3}
            value={newExperience.responsibility}
            onChange={(e) =>
              setNewExperience((prev) => {
                return {
                  ...prev,
                  responsibility: e.target.value,
                };
              })
            }
          />
          <Form.Label>From</Form.Label>
          <Form.Control
            type="date"
            value={newExperience.from}
            onChange={(e) =>
              setNewExperience((prev) => {
                return {
                  ...prev,
                  from: e.target.value,
                };
              })
            }
          />
          <Form.Label>To</Form.Label>
          <Form.Control
            type="date"
            value={newExperience.to}
            onChange={(e) =>
              setNewExperience((prev) => {
                return {
                  ...prev,
                  to: e.target.value,
                };
              })
            }
          />
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={() => setShowAddExperience(false)}
          >
            Close
          </Button>
          <Button variant="primary" onClick={handleAddExperience}>
            Add new
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal
        show={showEditExperience}
        onHide={() => setShowEditExperience(false)}
      >
        <Modal.Header closeButton>
          <Modal.Title>Edit your working experience</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ paddingLeft: "30px" }}>
          <Form.Label>Company name</Form.Label>
          <Form.Control
            value={editingExperience.companyName}
            onChange={(e) =>
              setEditingExperience((prev) => {
                return {
                  ...prev,
                  companyName: e.target.value,
                };
              })
            }
          />
          <Form.Label>Position</Form.Label>
          <Form.Control
            value={editingExperience.position}
            onChange={(e) =>
              setEditingExperience((prev) => {
                return {
                  ...prev,
                  position: e.target.value,
                };
              })
            }
          />
          <Form.Label>Responsibility</Form.Label>
          <Form.Control
            as="textarea"
            rows={6}
            value={editingExperience.responsibility}
            onChange={(e) =>
              setEditingExperience((prev) => {
                return {
                  ...prev,
                  responsibility: e.target.value,
                };
              })
            }
          />
          <Form.Label>From</Form.Label>
          <Form.Control
            type="date"
            value={
              editingExperience.from &&
              formatDateToYYYYMMDD(editingExperience.from)
            }
            onChange={(e) =>
              setEditingExperience((prev) => {
                return {
                  ...prev,
                  from: e.target.value,
                };
              })
            }
          />
          <Form.Label>To</Form.Label>
          <Form.Control
            type="date"
            value={
              editingExperience.to && formatDateToYYYYMMDD(editingExperience.to)
            }
            onChange={(e) =>
              setEditingExperience((prev) => {
                return {
                  ...prev,
                  to: e.target.value,
                };
              })
            }
          />
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={() => setShowEditExperience(false)}
          >
            Cancel
          </Button>
          <Button variant="primary" onClick={handleEditExperience}>
            Save
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal show={showAddSkill} onHide={() => setShowAddSkill(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Add new skill</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ paddingLeft: "30px" }}>
          <Form.Label>Skill</Form.Label>
          <Form.Control
            value={newSkill.title}
            onChange={(e) =>
              setNewSkill((prev) => {
                return {
                  ...prev,
                  title: e.target.value,
                };
              })
            }
          />
          <Form.Label>Score</Form.Label>
          <Form.Control
            value={newSkill.score}
            onChange={(e) =>
              setNewSkill((prev) => {
                return {
                  ...prev,
                  score: e.target.value,
                };
              })
            }
          />
          <Form.Label>Cert link</Form.Label>
          <Form.Control
            value={newSkill.certificateLink}
            onChange={(e) =>
              setNewSkill((prev) => {
                return {
                  ...prev,
                  certificateLink: e.target.value,
                };
              })
            }
          />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowAddSkill(false)}>
            Close
          </Button>
          <Button variant="primary" onClick={handleAddSkill}>
            Add new
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal show={showEditSkill} onHide={() => setShowEditSkill(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Edit your skill</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ paddingLeft: "30px" }}>
          <Form.Label>Skill</Form.Label>
          <Form.Control
            value={editingSkill.title}
            onChange={(e) =>
              setEditingSkill((prev) => {
                return {
                  ...prev,
                  title: e.target.value,
                };
              })
            }
          />
          <Form.Label>Score</Form.Label>
          <Form.Control
            value={editingSkill.score}
            onChange={(e) =>
              setEditingSkill((prev) => {
                return {
                  ...prev,
                  score: e.target.value,
                };
              })
            }
          />
          <Form.Label>Cert link</Form.Label>
          <Form.Control
            value={editingSkill.certificateLink}
            onChange={(e) =>
              setEditingSkill((prev) => {
                return {
                  ...prev,
                  certificateLink: e.target.value,
                };
              })
            }
          />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowEditSkill(false)}>
            Cancel
          </Button>
          <Button variant="primary" onClick={handleEditSkill}>
            Save
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal size="lg" show={showMyCV} onHide={() => setShowMyCV(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Your Resume</Modal.Title>
        </Modal.Header>
        <Modal.Body
          style={{ paddingLeft: "30px", maxHeight: "80rem", overflow: "auto" }}
        >
          <span className="small text-danger">
            *Only Resume created by our system can be edited
          </span>
          <table className="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Create Time</th>
                <th scope="col">Title</th>
                <th scope="col"></th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
              {myCVList.map((resume, index) => (
                <tr key={index}>
                  <th scope="row">{index + 1}</th>
                  <td>{formatTime(resume.createdAt)}</td>
                  <td>
                    <a
                      className="badge bg-dark bg-opacity-50"
                      href={resume.link}
                      target="_blank"
                    >
                      {resume.title}
                    </a>
                  </td>
                  <td>
                    {resume.method === "create" && (
                      <button
                        className="btn btn-delete btn-warning"
                        onClick={() => handleEditResume(resume.id)}
                      >
                        Edit
                      </button>
                    )}
                  </td>
                  <td>
                    <button
                      className="btn btn-delete btn-danger"
                      onClick={() => handleDeleteResume(resume.id)}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="info"
            onClick={() => navigate(`/create-resume/${employeeId}`)}
          >
            Create new
          </Button>
          <Button
            variant="success"
            onClick={() => {
              setShowResumeTitle(true);
              setShowMyCV(false);
            }}
          >
            Upload new
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal show={showResumeTitle} onHide={() => setShowResumeTitle(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Upload New Resume</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ paddingLeft: "30px" }}>
          <Form.Label>Resume Title</Form.Label>
          <Form.Control
            className="form-control mb-2"
            value={resumeTitle}
            onChange={(e) =>
              setResumeTitle((prev) => {
                return e.target.value;
              })
            }
          />
          <Form.Label>Resume File</Form.Label>
          <input
            className="form-control"
            type="file"
            ref={InputCVRef}
            placeholder="Upload avatar"
            accept="image/png,image/jpeg,image/jpg,image/gif, .pdf"
            defaultValue={""}
            onChange={(e) => handleUploadCV(e)}
          />
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={() => {
              setShowResumeTitle(false);
              setResumeTitle("");
              setFile(null);
            }}
          >
            Close
          </Button>
          <Button variant="primary" onClick={() => handleUploadResume()}>
            Save
          </Button>
        </Modal.Footer>
      </Modal>

      <Application
        show={showApplication}
        onHide={() => setShowApplication(false)}
        modalTitle={"My Application"}
        applicationList={myApplicationList}
        isOwner={false}
      />
    </>
  );
});

export default memo(EmployeeProfile);
