import { memo, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import EmployeeProfile from "./EmployeeProfile";
import CompanyProfile from "./CompanyProfile";
import Navbar from "../../components/Navbar/Navbar";
import Footer from "../../components/Footer/Footer";
import { useAppDispatch, useAppSelector } from "../../redux/hooks";
import { Company, Employee } from "../../ultils/type";
import { editEmployerProfile, getEmployerProfile } from "./CompanyApi";
import { getEmployeeProfile, editEmployeeProfile } from "./EmployeeApi";
import { setCompanyProfile, setEmployeeProfile } from "./ProfileSlice";
import { toast } from "react-toastify";

function Profile() {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const currentUser = useAppSelector((store) => store.login.userInfo);
  const { id = currentUser.id } = useParams();

  useEffect(() => {
    if (!currentUser) {
      navigate("/");
    }

    if (currentUser.role === "employer") {
      dispatch(getEmployerProfile({ id: id }));
    }
    if (currentUser.role === "employee") {
      dispatch(getEmployeeProfile({ id: id }));
    }
  }, [currentUser, dispatch, id, navigate]);

  const profileCompany = useAppSelector(
    (store) => store.profile.companyProfile
  );
  const profileEmployee = useAppSelector(
    (store) => store.profile.employeeProfile
  );

  const [isScrolled, setIsScrolled] = useState(true);
  window.onscroll = () => {
    setIsScrolled(window.scrollY === 0 ? true : false);
    return () => (window.onscroll = null);
  };

  const editCompanyProfile = async (company: Company) => {
    try {
      const res = await dispatch(
        editEmployerProfile({
          name: company.name,
          description: company.description,
          logoLink: company.logoLink,
          websiteLink: company.websiteLink,
          locations: company.locations,
        })
      );

      if (res.payload) {
        dispatch(setCompanyProfile(company));
        toast.success("Edit information successfully");
      }
    } catch (err) {
      toast.error(String(err));
    }
  };

  const editEmployeeInfo = async (employee: Employee) => {
    try {
      const res = await dispatch(
        editEmployeeProfile({
          name: employee.name,
          avatar: employee.avatar,
          email: employee.email,
          gender: employee.gender,
          dateOfBirth: employee.dateOfBirth,
          location: employee.location,
          linkedInLink: employee.linkedInLink,
          gitHubLink: employee.gitHubLink,
        })
      );

      if (res.payload) {
        dispatch(setEmployeeProfile(employee));
        toast.success("Edit information successfully");
      }
    } catch (err) {
      toast.error(String(err));
    }
  };

  return (
    <>
      <Navbar isScrolled={isScrolled} />
      {currentUser.role === "employee"
        ? id && (
            <EmployeeProfile
              employeeId={id}
              employee={profileEmployee}
              handleChangeEmployeeProfile={editEmployeeInfo}
            />
          )
        : id && (
            <CompanyProfile
              companyId={id}
              company={profileCompany}
              handleChangeCompanyProfile={editCompanyProfile}
            />
          )}
      <Footer />
    </>
  );
}

export default memo(Profile);
