import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import {
  editEmployerJob,
  getApplicationList,
  getEmployerJob,
  getEmployerProfile,
  replyApplication,
} from "./CompanyApi";
import {
  Application,
  Company,
  Education,
  Employee,
  Job,
  Resume,
  Skill,
  WorkingExperience,
  EApplicationStatus,
} from "../../ultils/type";
import {
  deleteEmployeeResume,
  getEmployeeApplication,
  getEmployeeProfile,
  getMyCV,
} from "./EmployeeApi";

export interface ProfileState {
  companyProfile: Company;
  currentCompanyJob: Job[];
  employeeProfile: Employee;
  myCV: Resume[];
  applicationList: Application[];
  selectedJob: Job | null;
  employeeApplyList: Application[];
}
const initialState: ProfileState = {
  companyProfile: {} as Company,
  currentCompanyJob: [],
  employeeProfile: {} as Employee,
  myCV: [],
  applicationList: [],
  selectedJob: null,
  employeeApplyList: [],
};

export const ProfileSlice = createSlice({
  name: "profile",
  initialState,
  reducers: {
    setCompanyProfile: (state, action: PayloadAction<Company>) => {
      state.companyProfile = action.payload;
    },
    setEmployeeProfile: (state, action: PayloadAction<Employee>) => {
      state.employeeProfile = action.payload;
    },
    addJob: (state, action: PayloadAction<Job>) => {
      state.currentCompanyJob = [action.payload, ...state.currentCompanyJob];
    },
    deleteJob: (state, action: PayloadAction<string>) => {
      state.currentCompanyJob = state.currentCompanyJob.filter(
        (job) => job.id !== action.payload
      );
    },
    changeStateJob: (state, action: PayloadAction<string>) => {
      state.currentCompanyJob = state.currentCompanyJob.map((job) => {
        if (job.id === action.payload) {
          return { ...job, active: !job.active };
        } else return job;
      });
    },
    addEdu: (state, action: PayloadAction<Education>) => {
      let newEdu = [...state.employeeProfile.education, action.payload];
      state.employeeProfile = { ...state.employeeProfile, education: newEdu };
    },
    editEdu: (state, action: PayloadAction<Education>) => {
      state.employeeProfile.education = state.employeeProfile.education.map(edu => edu.id === action.payload.id ? action.payload : edu)
    },
    editExperience: (state, action: PayloadAction<WorkingExperience>) => {
      state.employeeProfile.workingExperience = state.employeeProfile.workingExperience.map(ex => ex.id === action.payload.id ? action.payload : ex)
    },
    editSkill: (state, action: PayloadAction<Skill>) => {
      state.employeeProfile.skill = state.employeeProfile.skill.map(sk => sk.id === action.payload.id ? action.payload : sk)
    },
    deleteEdu: (state, action: PayloadAction<string>) => {
      let newEdu = state.employeeProfile.education.filter(
        (edu) => edu.id !== action.payload
      );
      state.employeeProfile = { ...state.employeeProfile, education: newEdu };
    },
    addExperience: (state, action: PayloadAction<WorkingExperience>) => {
      let newExperience = [
        ...state.employeeProfile.workingExperience,
        action.payload,
      ];
      state.employeeProfile = {
        ...state.employeeProfile,
        workingExperience: newExperience,
      };
    },
    deleteExperience: (state, action: PayloadAction<string>) => {
      let newExperience = state.employeeProfile.workingExperience.filter(
        (exper) => exper.id !== action.payload
      );
      state.employeeProfile = {
        ...state.employeeProfile,
        workingExperience: newExperience,
      };
    },
    addSkill: (state, action: PayloadAction<Skill>) => {
      let newSkill = [...state.employeeProfile.skill, action.payload];
      state.employeeProfile = { ...state.employeeProfile, skill: newSkill };
    },
    deleteSkill: (state, action: PayloadAction<string>) => {
      let newSkill = state.employeeProfile.skill.filter(
        (sk) => sk.id !== action.payload
      );
      state.employeeProfile = { ...state.employeeProfile, skill: newSkill };
    },
    setSelectedJob: (state, action: PayloadAction<Job>) => {
      state.selectedJob = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getEmployerProfile.fulfilled, (state, action) => {
      state.companyProfile = action.payload;
    });
    builder.addCase(getEmployeeProfile.fulfilled, (state, action) => {
      state.employeeProfile = action.payload;
    });
    builder.addCase(getEmployerJob.fulfilled, (state, action) => {
      state.currentCompanyJob = action.payload;
    });
    builder.addCase(getMyCV.fulfilled, (state, action) => {
      state.myCV = action.payload;
    });
    builder.addCase(getApplicationList.fulfilled, (state, action) => {
      state.applicationList = action.payload;
    });
    builder.addCase(replyApplication.fulfilled, (state, action) => {
      let { applicationId, status } = action.meta.arg;
      let currentApplication = state.applicationList.find(
        (app) => app.id === applicationId
      );
      if (currentApplication) {
        let newApplication = {
          ...currentApplication,
          status:
            status === "accepted"
              ? EApplicationStatus.accepted
              : status === "rejected"
                ? EApplicationStatus.rejected
                : EApplicationStatus.pending,
        };
        state.applicationList = state.applicationList.map((app) =>
          app.id === applicationId ? newApplication : app
        );
      }
    });
    builder.addCase(getEmployeeApplication.fulfilled, (state, action) => {
      state.employeeApplyList = action.payload;
    });
    builder.addCase(deleteEmployeeResume.fulfilled, (state, action) => {
      state.myCV = state.myCV.filter(
        (resume) => resume.id !== action.meta.arg.id
      );
    });
    builder.addCase(editEmployerJob.fulfilled, (state, action) => {
      state.currentCompanyJob = state.currentCompanyJob.map((job) => {
        if (job.id === action.meta.arg.id) {
          return action.meta.arg;
        } else return job;
      });
    });
  },
});

export const {
  setCompanyProfile,
  setEmployeeProfile,
  addJob,
  deleteJob,
  changeStateJob,
  addEdu,
  deleteEdu,
  addExperience,
  deleteExperience,
  addSkill,
  deleteSkill,
  setSelectedJob,
  editEdu,
  editExperience,
  editSkill
} = ProfileSlice.actions;
export default ProfileSlice.reducer;
