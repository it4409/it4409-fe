import { createSlice } from "@reduxjs/toolkit";
import { Company, Employee } from "../../../ultils/type";
import { getEmployeeProfileView, getEmployerProfileView } from "./ProfileViewApi";

export interface ProfileViewState {
    profileView: Company | Employee | null
}
const initialState: ProfileViewState = {
    profileView: null
};

export const ProfileViewSlice = createSlice({
    name: "profile-view",
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(getEmployerProfileView.fulfilled, (state, action) => {
            state.profileView = action.payload;
        });
        builder.addCase(getEmployeeProfileView.fulfilled, (state, action) => {
            state.profileView = action.payload;
        });

    },
});

export default ProfileViewSlice.reducer;
