import { createAsyncThunk } from "@reduxjs/toolkit";
import axiosWithAuth from "../../../axiosInstance";
import { BASE_URL } from "../../../ultils/constant";

export const getEmployerProfileView = createAsyncThunk<any, { id: string }>(
    "/get-employer-info-view",
    async (payload) => {
        return await (
            await axiosWithAuth.get(`${BASE_URL}/employer-info?userId=${payload.id}`)
        ).data;
    }
);

export const getEmployeeProfileView = createAsyncThunk<any, { id: string }>(
    "/get-employee-info-view",
    async (payload) => {
        return await (
            await axiosWithAuth.get(`${BASE_URL}/employee-info?userId=${payload.id}`)
        ).data;
    }
);