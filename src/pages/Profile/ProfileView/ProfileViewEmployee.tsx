import React, { useEffect, useState } from "react";
import { Button, Modal } from "react-bootstrap";
import { useNavigate, useParams } from "react-router-dom";
import Footer from "../../../components/Footer/Footer";
import Navbar from "../../../components/Navbar/Navbar";
import { useAppDispatch, useAppSelector } from "../../../redux/hooks";
import { Employee } from "../../../ultils/type";
import { formatDate, formatDateToMMYYYY } from "../../../utils";
import NotFound from "../../NotFound/NotFound";
import { getEmployeeProfileView } from "./ProfileViewApi";

const ProfileViewEmployee = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const dispatch = useAppDispatch();

  const loginState = useAppSelector((store) => store.login);
  const profileView = useAppSelector(
    (store) => store.profileView.profileView
  ) as Employee | null;

  const [isScrolled, setIsScrolled] = useState(true);
  window.onscroll = () => {
    setIsScrolled(window.scrollY === 0 ? true : false);
    return () => (window.onscroll = null);
  };

  useEffect(() => {
    if (!loginState.isLogin || loginState.userInfo.role !== "employer") {
      navigate("/");
    }
    if (profileView) {
      return;
    } else if (id) {
      dispatch(getEmployeeProfileView({ id: id }));
    }
  }, [dispatch, id, loginState, navigate, profileView]);

  const [showContact, setShowContact] = useState(false);

  if (profileView) {
    return (
      <>
        <Navbar isScrolled={isScrolled} />
        <div
          className="bg-dark"
          style={{ minHeight: "500px", height: "fit-content", padding: "50px" }}
        >
          <div className="basic-info container">
            <div className="row">
              <div className="col-3 avatar">
                {!!profileView && !!profileView.avatar && (
                  <img
                    src={profileView.avatar}
                    alt="avatar"
                    style={{
                      width: "100%",
                      height: "100%",
                    }}
                  />
                )}
              </div>
              <div className="col-9">
                <div className="row" style={{ margin: "30px 50px" }}>
                  <div className="d-flex gap-4 align-items-center">
                    <div className="name">{profileView.name}</div>
                  </div>

                  {profileView.gender && (
                    <div className="col-12 gender">{profileView.gender}</div>
                  )}
                  {profileView.dateOfBirth && (
                    <div className="col-12 birth-day">
                      {formatDate(profileView.dateOfBirth)}
                    </div>
                  )}
                  {profileView.location && (
                    <div className="col-12 location">
                      Address: {profileView.location}
                    </div>
                  )}
                  <div className="mt-5">
                    <Button
                      className="col-4 contact "
                      onClick={() => setShowContact(true)}
                    >
                      Contact Information
                    </Button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <hr style={{ color: "white" }}></hr>

          <div className="container education ">
            {/* Show education */}
            <div className="d-flex gap-5">
              <div className="title ">Educations</div>
            </div>

            {!!profileView.education && !!profileView.education.length ? (
              profileView.education.map((edu, index) => (
                <div className="row" key={index}>
                  <div
                    className="col-2"
                    style={{ borderLeft: "1px solid #fff" }}
                  >
                    <div className="row">
                      <div className="col-12 time text-center">
                        {edu.from && formatDateToMMYYYY(edu.from)} -
                        {edu.to && formatDateToMMYYYY(edu.to)}
                      </div>
                    </div>
                  </div>
                  <div
                    className="col-9"
                    style={{ borderLeft: "1px solid #fff" }}
                  >
                    <div className="row">
                      <div className="col-12 school-name">{edu.schoolName}</div>
                      <div className="col-12 major">{edu.major}</div>
                    </div>
                  </div>
                  <hr />
                </div>
              ))
            ) : (
              <div className="d-flex justify-content-center text-center w-100 mt-4">
                <h4 style={{ fontWeight: "600", fontSize: "20px" }}>
                  No education information was found.
                </h4>
              </div>
            )}
          </div>

          <hr style={{ color: "white" }}></hr>
          <div className="container working">
            {/* Show working */}{" "}
            <div className="d-flex gap-5">
              <div className="title ">Working Experiences</div>
            </div>
            {!!profileView.workingExperience &&
            profileView.workingExperience.length ? (
              profileView.workingExperience?.map((work, index) => (
                <div className="row" key={index}>
                  <div
                    className="col-2"
                    style={{ borderLeft: "1px solid #fff" }}
                  >
                    <div className="row">
                      <div className="col-12 time text-center">
                        {work.from && formatDateToMMYYYY(work.from)} -{" "}
                        {work.to && formatDateToMMYYYY(work.to)}
                      </div>
                    </div>
                  </div>
                  <div
                    className="col-9"
                    style={{ borderLeft: "1px solid #fff" }}
                  >
                    <div className="row">
                      <div className="col-12 company-name">
                        {work.companyName}
                      </div>
                      <div className="col-12 position">{work.position}</div>
                      <div className="col-12 responsibility">
                        <pre>{work.responsibility}</pre>
                      </div>
                    </div>
                  </div>
                  <hr />
                </div>
              ))
            ) : (
              <div className="d-flex justify-content-center text-center w-100 mt-4">
                <h4 style={{ fontWeight: "600", fontSize: "20px" }}>
                  No working experience information was found.
                </h4>
              </div>
            )}
          </div>

          <hr style={{ color: "white" }}></hr>
          <div className="container skill">
            {/* Show skill */}
            <div className="d-flex gap-5">
              <div className="title ">Skills</div>
            </div>
            {!!profileView.skill && profileView.skill.length ? (
              profileView.skill?.map((skill, index) => (
                <div className="row d-flex justify-content-between" key={index}>
                  {!!skill.score && (
                    <div
                      className="col-2"
                      style={{ borderLeft: "1px solid #fff" }}
                    >
                      <div className="row">
                        <div className="col-12 score">SCORE: {skill.score}</div>
                      </div>
                    </div>
                  )}
                  <div
                    className="col-7"
                    style={{ borderLeft: "1px solid #fff" }}
                  >
                    <div className="row">
                      <div className="col-12 skill-name">{skill.title}</div>
                      {!!skill.certificateLink && (
                        <div className="col-12 certificateLink">
                          Certificate-link:
                          {"  " + skill?.certificateLink}
                        </div>
                      )}
                    </div>
                  </div>
                  <hr />
                </div>
              ))
            ) : (
              <div className="d-flex justify-content-center text-center w-100 mt-4">
                <h4 style={{ fontWeight: "600", fontSize: "20px" }}>
                  No skill information was found.
                </h4>
              </div>
            )}
          </div>
        </div>

        <Modal show={showContact} onHide={() => setShowContact(false)}>
          <Modal.Header closeButton>
            <Modal.Title>{profileView.name}</Modal.Title>
          </Modal.Header>
          <Modal.Body style={{ paddingLeft: "30px" }}>
            <div className="row">Email: {profileView.email}</div>
            <div className="row">Mobile Phone: {profileView.mobilePhone}</div>
            <div className="row">Linkedin: {profileView.linkedInLink}</div>
            <div className="row">Github: {profileView.gitHubLink}</div>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={() => setShowContact(false)}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
        <div className="bg-light flex j-center p-0" style={{ height: "2rem" }}>
          You're in view mode. You can only view this profile. &nbsp;
          <p
            className="text-decoration-underline"
            style={{ cursor: "pointer" }}
            onClick={() => navigate(-1)}
          >
            Go back
          </p>
        </div>
        <Footer />
      </>
    );
  } else return <NotFound />;
};

export default ProfileViewEmployee;
