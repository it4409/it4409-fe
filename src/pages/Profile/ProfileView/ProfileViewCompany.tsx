import React, { useEffect, useState } from "react";
import { Button, Modal } from "react-bootstrap";
import { useNavigate, useParams } from "react-router-dom";
import Footer from "../../../components/Footer/Footer";
import Navbar from "../../../components/Navbar/Navbar";
import { useAppDispatch, useAppSelector } from "../../../redux/hooks";
import { Company } from "../../../ultils/type";
import { searchJob } from "../../Jobs/JobsApi";
import { setSearchConditions } from "../../Jobs/JobSlice";
import NotFound from "../../NotFound/NotFound";
import { getEmployerProfileView } from "./ProfileViewApi";

const ProfileViewCompany = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const dispatch = useAppDispatch();

  const isLogin = useAppSelector((store) => store.login.isLogin);
  const profileView = useAppSelector(
    (store) => store.profileView.profileView
  ) as Company | null;

  const [isScrolled, setIsScrolled] = useState(true);
  window.onscroll = () => {
    setIsScrolled(window.scrollY === 0 ? true : false);
    return () => (window.onscroll = null);
  };

  useEffect(() => {
    if (!isLogin) {
      navigate("/");
    }
    if (profileView) {
      return;
    } else if (id) {
      dispatch(getEmployerProfileView({ id: id }));
    }
  }, [dispatch, id, isLogin, navigate, profileView]);

  const [showContact, setShowContact] = useState(false);

  const getJobsByCompany = async (company: Company) => {
    await dispatch(searchJob({ companyId: company.id }));
    dispatch(setSearchConditions([company.name]));
    navigate("/result");
  };

  if (profileView) {
    return (
      <>
        <Navbar isScrolled={isScrolled} />
        <div
          className="bg-dark"
          style={{ minHeight: "500px", height: "fit-content", padding: "50px" }}
        >
          <div className="basic-info container">
            <div className="row">
              <div className="col-3 avatar">
                <img
                  src={profileView!.logoLink || ""}
                  alt="avatar"
                  style={{
                    width: "100%",
                    height: "100%",
                  }}
                />
              </div>
              <div className="col-6">
                <div className="row" style={{ margin: "30px 50px" }}>
                  <div className="d-flex gap-5 align-items-center">
                    <div className=" name">{profileView!.name}</div>
                  </div>
                  {!!profileView!.websiteLink && (
                    <div className="col-12 website">
                      {profileView!.websiteLink}
                    </div>
                  )}
                  {!!profileView!.locations &&
                    !!profileView!.locations.length && (
                      <div className="col-12 locations">
                        Address: &nbsp;
                        {profileView!.locations?.map((loc) => loc).join("| ")}
                      </div>
                    )}
                  <Button
                    className="col-8 contact mt-5"
                    onClick={() => setShowContact(true)}
                  >
                    Contact Information
                  </Button>
                </div>
              </div>
            </div>
            <div className="row mt-2">
              <div className="col-12 description mb-5">
                {profileView!.email}
              </div>
              {profileView!.description && (
                <div className="col-12 description mb-5">
                  <pre>{profileView!.description}</pre>
                </div>
              )}
            </div>
            <hr />
            <Button
              className="btn btn-info"
              onClick={() => getJobsByCompany(profileView)}
            >
              See Jobs
            </Button>
          </div>
        </div>

        <Modal show={showContact} onHide={() => setShowContact(false)}>
          <Modal.Header closeButton>
            <Modal.Title>{profileView!.name}</Modal.Title>
          </Modal.Header>
          <Modal.Body style={{ paddingLeft: "30px" }}>
            <div className="row">Email: {profileView!.email}</div>
            <div className="row">Mobile Phone: {profileView!.mobilePhone}</div>
            <div className="row">Website: {profileView!.websiteLink}</div>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={() => setShowContact(false)}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
        <div className="bg-light flex j-center p-0" style={{ height: "2rem" }}>
          You're in view mode. You can only view this company profile. &nbsp;
          <p
            className="text-decoration-underline"
            style={{ cursor: "pointer" }}
            onClick={() => navigate(-1)}
          >
            Go back
          </p>
        </div>
        <Footer />
      </>
    );
  } else return <NotFound />;
};

export default ProfileViewCompany;
