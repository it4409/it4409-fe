import React, { memo, useState } from "react";
import Footer from "../../components/Footer/Footer";
import Navbar from "../../components/Navbar/Navbar";
import { useAppSelector } from "../../redux/hooks";
import Jobs from "../Jobs/Jobs";
import { useNavigate } from "react-router-dom";

const Result = () => {
  const result = useAppSelector((store) =>
    store.job.searchResult.filter((job) => job.active)
  );
  const conditions = useAppSelector((store) => store.job.searchConditions);

  const navigate = useNavigate();
  if (conditions.every((dk) => dk === "")) navigate("/");

  const [isScrolled, setIsScrolled] = useState(true);
  window.onscroll = () => {
    setIsScrolled(window.scrollY === 0 ? true : false);
    return () => (window.onscroll = null);
  };

  return (
    <div
      className="bg-dark text-light"
      style={{ paddingTop: "8rem", minHeight: "100vh" }}
    >
      <Navbar isScrolled={isScrolled} />
      <div className="container">
        <h1>
          There is {result.length} job{result.length !== 1 && "s"} available for{" "}
          {conditions.map(
            (cdt, index) =>
              cdt && (
                <>
                  <span className="border border-info rounded py-1 px-2 m-1">
                    {cdt}
                  </span>
                  &nbsp;
                </>
              )
          )}
        </h1>
        <br />
        <Jobs list={result} />
      </div>
      <Footer />
    </div>
  );
};

export default memo(Result);
