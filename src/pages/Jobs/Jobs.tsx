import React, { memo, useState } from "react";
import { useNavigate } from "react-router-dom";
import "./Jobs.scss";
import { Job } from "../../ultils/type";
import { useAppDispatch } from "../../redux/hooks";
import { searchJob } from "./JobsApi";
import { setSearchConditions } from "./JobSlice";

interface JobsProps {
  list: Job[] | null;
}

const Jobs = (props: JobsProps) => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();

  const [selectedJob, setselectedJob] = useState<Job | null>(null);

  const handleSearchByKeyWord = async (keyword: string) => {
    await dispatch(searchJob({ keyword: keyword }));
    dispatch(setSearchConditions([keyword]));
    navigate("/result");
  };
  const handleSearchByLocation = async (location: string) => {
    await dispatch(searchJob({ locations: [location] }));
    dispatch(setSearchConditions([location]));
    navigate("/result");
  };

  return (
    <div className="jobContainer row">
      <div className="jobList col-4 border p-0">
        {props.list &&
          props.list.map((job) => {
            return (
              job.active && (
                <div
                  className="jobContent"
                  key={job.id}
                  onClick={() => setselectedJob(job)}
                >
                  <div className="row p-2 m-0">
                    <div className="col-3">
                      <img
                        src={
                          job.companyLogo ? job.companyLogo : ".imageslogo.webp"
                        }
                        style={{
                          maxWidth: "100%",
                          maxHeight: "100%",
                        }}
                        alt="job-img"
                      />
                    </div>
                    <div className="col-9">
                      <h4>{job.title}</h4>
                      <p>${job.salary}</p>
                      {job.keywords.map((keyword, index) => {
                        return (
                          <span
                            className="badge badge-pill bg-success m-1"
                            key={index}
                            onClick={() => handleSearchByKeyWord(keyword.title)}
                          >
                            {keyword.title}
                          </span>
                        );
                      })}
                      {job.locations.map((location, index) => {
                        return (
                          <span
                            className="badge badge-pill bg-primary m-1"
                            key={index}
                            onClick={() => handleSearchByLocation(location)}
                          >
                            {location}
                          </span>
                        );
                      })}
                    </div>
                  </div>
                </div>
              )
            );
          })}
      </div>
      <div className="jobDetail col-8 border">
        {selectedJob ? (
          <div className="detail">
            <h1>{selectedJob.title}</h1>
            <br />
            <button
              className="w-100 btn btn-info"
              onClick={() => navigate(`/job-detail/${selectedJob.id}`)}
            >
              See Details
            </button>
            <hr />
            <div>
              Job Requirements : <pre>{selectedJob.requirement}</pre>
              {selectedJob.keywords &&
                selectedJob.keywords.map((keyword, index) => {
                  return (
                    <p key={index}>
                      - {keyword.title}{" "}
                      {keyword.score ? `: ${keyword.score}` : ""}
                    </p>
                  );
                })}
            </div>
            <br />
            <p>Salary : up to ${selectedJob.salary}</p>
            <br />
            <div>
              Locate at :
              {selectedJob.locations &&
                selectedJob.locations.map((location, index) => (
                  <span
                    className="badge badge-pill bg-primary me-1 opacity-75"
                    key={index}
                  >
                    {location}
                  </span>
                ))}
            </div>
            <hr />
            <h2>Job Description</h2>
            <br />
            <pre>{selectedJob.description}</pre>
          </div>
        ) : (
          <div
            className="text-center text-muted mt-2"
            style={{ fontSize: "1.5rem", fontStyle: "italic" }}
          >
            Select a job to see details
          </div>
        )}
      </div>
    </div>
  );
};

export default memo(Jobs);
