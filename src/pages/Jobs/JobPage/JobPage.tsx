import React, { memo, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Footer from "../../../components/Footer/Footer";
import Navbar from "../../../components/Navbar/Navbar";
import { useAppDispatch, useAppSelector } from "../../../redux/hooks";
import { getJobDetail, searchJob } from "../JobsApi";
import { Company, Resume } from "../../../ultils/type";
import { setSearchConditions } from "../JobSlice";
import { toast } from "react-toastify";
import { applyJob, getMyCV } from "../../Profile/EmployeeApi";
import { Button, Modal } from "react-bootstrap";
import { formatDate } from "../../../utils";
import { getEmployerProfileView } from "../../Profile/ProfileView/ProfileViewApi";

const JobPage = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const currentUser = useAppSelector((store) => store.login.userInfo);
  const resumeList = useAppSelector((store) => store.profile.myCV);
  const jobDetail = useAppSelector((store) => store.job.selectedJob);
  const jobOwner = useAppSelector((store) =>
    store.company.companyList.find((c) => c.id === jobDetail.companyId)
  );
  const [showAddResume, setShowAddResume] = useState(false);
  const [showChooseResume, setShowChooseResume] = useState(false);
  const [applyResume, setApplyResume] = useState<Resume | null>(null);

  const [isScrolled, setIsScrolled] = useState(true);
  window.onscroll = () => {
    setIsScrolled(window.scrollY === 0 ? true : false);
    return () => (window.onscroll = null);
  };

  const { id = jobDetail.id } = useParams();

  useEffect(() => {
    dispatch(getJobDetail({ id }));
  }, [dispatch, id]);

  useEffect(() => {
    if (currentUser.role === "employee") {
      dispatch(getMyCV({ id: currentUser.id }));
    }
  }, [currentUser, dispatch]);

  const getJobsByCompany = async (company: Company) => {
    await dispatch(searchJob({ companyId: company.id }));
    dispatch(setSearchConditions([company.name]));
    navigate("/result");
  };

  const handleApplyJob = async () => {
    if (applyResume) {
      let res = await dispatch(
        applyJob({
          jobPostId: jobDetail.id,
          resumeId: applyResume.id,
        })
      );
      if (res.meta.requestStatus === "fulfilled") {
        toast.success("Apply job successfully");
        setShowChooseResume(false);
      } else if (res.meta.requestStatus === "rejected") {
        toast.error("Apply job failed");
      }
    } else {
      toast.error("No Resume selected");
    }
  };

  return (
    <div className="bg-dark" style={{ color: "white", paddingTop: "8rem" }}>
      <Navbar isScrolled={isScrolled} />
      <div className="container border rounded p-4">
        <div className="detail">
          <h1>{jobDetail.title}</h1>
          {jobDetail.keywords &&
            jobDetail.keywords.map((keyword, index) => {
              return (
                <span key={index} className="badge badge-pill bg-success m-2">
                  {keyword.title}
                </span>
              );
            })}
          <br />
          {currentUser.role === "employee" ? (
            <button
              className="w-100 btn btn-danger"
              onClick={() =>
                resumeList.length > 0
                  ? setShowChooseResume(true)
                  : setShowAddResume(true)
              }
            >
              Apply now
            </button>
          ) : (
            <button
              className="w-100 btn btn-danger"
              disabled={currentUser.role === "employer"}
              onClick={() => navigate("/login")}
            >
              Login as employee to apply the job
            </button>
          )}
          <hr />
          <div>
            Job Requirements : <pre>{jobDetail.requirement}</pre>
            <hr />
            {jobDetail.keywords &&
              jobDetail.keywords.map((keyword, index) => {
                return (
                  <p key={index}>
                    - {keyword.title}{" "}
                    {keyword.score ? `: ${keyword.score}` : ""}
                  </p>
                );
              })}
          </div>
          <br />
          <p>Salary : up to ${jobDetail.salary}</p>
          <br />
          <div>
            Locate at : &nbsp;
            {jobDetail.locations &&
              jobDetail.locations.map((location, index) => (
                <span
                  className="badge badge-pill bg-primary me-1 opacity-75"
                  key={index}
                >
                  {location}
                </span>
              ))}
          </div>
          <hr />
          <h2>Job Description</h2>
          <br />
          <pre>{jobDetail.description}</pre>
        </div>
        <hr />
        {jobOwner && (
          <>
            <div className="company m-1"> Company</div>
            <div className="row">
              <div className="col-sm-6 ">
                <div className="card">
                  <div className="card-body bg-dark">
                    <h5 className="card-title">{jobOwner?.name}</h5>
                    <pre className="card-text">{jobOwner?.description}</pre>
                    <a
                      href={jobOwner?.websiteLink}
                      className="card-text mb-2"
                      style={{ display: "block" }}
                    >
                      {jobOwner?.websiteLink}
                    </a>
                    <button
                      className="btn btn-primary"
                      onClick={() => jobOwner && getJobsByCompany(jobOwner)}
                    >
                      See Jobs by this company
                    </button>
                    <button
                      className="btn btn-warning"
                      style={{ marginLeft: "1rem" }}
                      onClick={async () => {
                        await dispatch(
                          getEmployerProfileView({ id: jobOwner.userId })
                        );
                        navigate(`/profile-view-company/${jobOwner.userId}`);
                      }}
                    >
                      About this company
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </>
        )}
      </div>
      <Footer />

      <Modal
        size="lg"
        show={showAddResume}
        onHide={() => setShowAddResume(false)}
      >
        <Modal.Header closeButton>
          <Modal.Title>You dont have any resume. Create or Upload?</Modal.Title>
        </Modal.Header>
        <Modal.Footer>
          <Button variant="info" onClick={() => navigate(`/create-resume`)}>
            Create New
          </Button>
          <Button
            variant="success"
            onClick={() => {
              navigate(`/info/${currentUser.id}`);
              toast.warning(`Click MyCV to add a new resume`);
            }}
          >
            Upload new
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal
        size="lg"
        show={showChooseResume}
        onHide={() => setShowChooseResume(false)}
      >
        <Modal.Header closeButton>
          <Modal.Title>Choose Resume to apply</Modal.Title>
        </Modal.Header>
        <Modal.Body
          style={{ paddingLeft: "30px", maxHeight: "80rem", overflow: "auto" }}
        >
          <table className="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Resume</th>
                <th scope="col">Create at</th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
              {resumeList.map((resume, index) => (
                <tr key={index}>
                  <th scope="row">{index}</th>
                  <td>
                    <a
                      className={`badge bg-dark bg-opacity-50`}
                      href={resume.link}
                      target="_blank"
                    >
                      {resume.title}
                    </a>
                    {applyResume && resume.id === applyResume.id && (
                      <span className="mx-1 badge bg-danger">selected</span>
                    )}
                  </td>
                  <td>{formatDate(resume.createdAt)}</td>
                  <td>
                    {!applyResume || resume.id !== applyResume.id ? (
                      <button
                        className="btn btn-select btn-sm rounded btn-primary"
                        onClick={() =>
                          setTimeout(() => {
                            setApplyResume(resume);
                          }, 300)
                        }
                      >
                        Choose
                      </button>
                    ) : (
                      ""
                    )}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </Modal.Body>
        <Modal.Footer>
          <button
            className="btn btn-delete btn-success"
            onClick={() => handleApplyJob()}
          >
            Confirm to apply
          </button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default memo(JobPage);
