import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from '@reduxjs/toolkit'
import { getJobDetail, getJobsList, searchJob } from "./JobsApi";
import { Job } from "../../ultils/type";
export interface JobState {
  jobList: Job[],
  jobListFetched: boolean,
  selectedJob: Job
  searchResult: Job[]
  searchConditions: string[]
}


const initialState: JobState = {
  jobList: [],
  jobListFetched: false,
  selectedJob: {} as Job,
  searchResult: [],
  searchConditions: ['']
}


export const jobSlice = createSlice({
  name: "job",
  initialState,
  reducers: {
    setJobList: (state, action: PayloadAction<Job[]>) => {
      state.jobList = action.payload;
    },
    setSearchConditions: (state, action: PayloadAction<string[]>) => {
      state.searchConditions = action.payload
    }
  },
  extraReducers: (builder) => {
    builder.addCase(getJobsList.fulfilled, (state, action) => {
      state.jobList = action.payload;
    })
    builder.addCase(getJobDetail.fulfilled, (state, action) => {
      state.selectedJob = action.payload
    })
    builder.addCase(searchJob.fulfilled, (state, action) => {
      state.searchResult = action.payload
    })
  }
});

export const { setJobList, setSearchConditions } = jobSlice.actions;
export default jobSlice.reducer;
