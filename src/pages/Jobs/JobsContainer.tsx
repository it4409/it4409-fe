import React, { memo, useEffect, useState } from "react";
import Footer from "../../components/Footer/Footer";
import Navbar from "../../components/Navbar/Navbar";
import { useAppDispatch, useAppSelector } from "../../redux/hooks";
import Jobs from "./Jobs";
import { Job } from "../../ultils/type";
import { getJobsList } from "./JobsApi";

const JobsContainer = () => {
  const dispatch = useAppDispatch();
  const jobsFetched = useAppSelector((store) => store.job.jobList);

  const [jobList, setjobList] = useState<Job[] | null>(null);
  const [isScrolled, setIsScrolled] = useState(true);
  window.onscroll = () => {
    setIsScrolled(window.scrollY === 0 ? true : false);
    return () => (window.onscroll = null);
  };

  useEffect(() => {
    dispatch(getJobsList());
  }, [dispatch]);

  useEffect(() => {
    setjobList(jobsFetched);
  }, [jobsFetched]);

  return (
    <div className="bg-dark" style={{ paddingTop: "9rem" }}>
      <Navbar isScrolled={isScrolled} />
      <div className="container content">
        <div className="form d-flex justify-content-start"></div>
        <div className="container p-2">
          <Jobs list={jobList} />
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default memo(JobsContainer);
