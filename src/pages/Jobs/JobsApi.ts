import { createAsyncThunk } from "@reduxjs/toolkit";
import { BASE_URL } from "../../ultils/constant";
import axios from "axios";
import { Job } from "../../ultils/type";

export const getJobsList = createAsyncThunk<Job[], void>(
    "/get-jobs",
    async () => {
        const res = (await axios.get(`${BASE_URL}/job`)).data
        return res
    }
);

export const getJobDetail = createAsyncThunk<Job, { id: string | undefined }>(
    "/get-job-detail",
    async (payload) => {
        const res = (await axios.get(`${BASE_URL}/job/${payload.id}`)).data
        return res
    }
);

export const searchJob = createAsyncThunk<
    Job[],
    {
        locations?: string[],
        keyword?: string,
        salary?: number,
        companyId?: string,
    }
>(
    "/search-job",
    async (payload) => {
        return (await axios.get(`${BASE_URL}/job`, {
            params: payload
        })).data
    }
);

