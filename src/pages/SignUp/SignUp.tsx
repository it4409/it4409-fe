import React, { useEffect, useState } from "react";
import "./SignUp.scss";
import "bootstrap/dist/css/bootstrap.min.css";
import { Link, useNavigate } from "react-router-dom";
import { FaEye, FaEyeSlash } from "react-icons/fa";
import { toast } from "react-toastify";
import {
  getUserInfo,
  loginAPI,
  registerAPI,
} from "../../services/authenService";
import { useDispatch } from "react-redux";
import { setLogin, setToken } from "../Login/LoginSlice";
import { toNamespacedPath } from "path";
import { AxiosError } from "axios";
// import { login } from "../../redux/actions/userAction";

const SignUp = () => {
  const [fullName, setFullName] = useState("");
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const [role, setRole] = useState("");
  const dispatch = useDispatch();
  const navigate = useNavigate();
  // validate
  const validate = () => {
    // fullname is optional, the rest is required
    if (!email || !username || !phoneNumber || !password || !confirmPassword) {
      toast.error("Please fill in all fields");
      return false;
    }

    // validate username is valid and at least 6 characters
    const reUsername = /^[a-zA-Z0-9]+$/;
    if (!reUsername.test(username) || username.length < 6) {
      toast.error("Username is invalid");
      return false;
    }

    // validate email
    const re = /\S+@\S+\.\S+/;
    if (!re.test(email)) {
      toast.error("Email is invalid");
      return false;
    }

    // validate phone number
    const rePhone =
      /\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$/;
    if (!rePhone.test(phoneNumber)) {
      toast.error("Phone number is invalid");
      return false;
    }

    // validate password at least 6 characters
    if (password.length < 6) {
      toast.error("Password must be at least 6 characters");
      return false;
    }

    if (password !== confirmPassword) {
      toast.error("Password does not match");
      return false;
    }

    // validate role
    if (!role) {
      toast.error("Please select your role");
      return false;
    }

    return true;
  };

  const handleSubmit = async (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();

    // convert if phone number is in wrong format to +84
    if (phoneNumber[0] === "0") {
      setPhoneNumber("+84" + phoneNumber.slice(1));
    }

    const isValid = validate();
    if (!isValid) return;
    try {
      const res = await registerAPI(
        fullName,
        email,
        username,
        phoneNumber,
        password,
        role
      );
      if (res.status === 201) {
        dispatch(
          setToken({
            token: {
              accessToken: res.data.accessToken,
              refreshToken: res.data.refreshToken,
            },
            isLogin: true,
          })
        );
        navigate("/");
        toast.success("Login successfully");
      } else {
        if (res?.data?.message) toast.error(res.data.message);
        else toast.error("Sign up failed");
      }
    } catch (error: any) {
      toast.error(error.response.data.message);
    }
  };

  // convert if phone number is in wrong format to +84
  useEffect(() => {
    if (phoneNumber[0] === "0") {
      setPhoneNumber("+84" + phoneNumber.slice(1));
    }
  }, [phoneNumber]);

  return (
    <>
      <div className="container-fluid signup-1">
        <div className="container">
          <div className="signup-form">
            <div className="row align-items-center">
              <div className="col-md-6">
                <div className="img-box">
                  <img
                    src="./images/login-img.png"
                    className="back-img"
                    title="signup"
                    alt="welcome"
                  />
                </div>
              </div>
              <div className="col-md-6">
                <div className="signup-box">
                  <form>
                    <div className="form-group">
                      <h4 className="signup-title">Register now!</h4>
                      <p className="signup-p">
                        Create a new account to finding your job
                      </p>
                    </div>
                    <div className="form-group">
                      <label className="signup-label">Full name</label>
                      <input
                        type="text"
                        value={fullName}
                        onChange={(e) => setFullName(e.target.value)}
                        placeholder="E.x: John Doe"
                        className="form-control"
                      />
                      <label className="signup-label">
                        Email <span>(*)</span>
                      </label>
                      <input
                        type="text"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        placeholder="E.x: timviecit@hotmail.com"
                        className="form-control"
                      />
                      <label className="signup-label">
                        Username <span>(*)</span>
                      </label>
                      <input
                        type="text"
                        value={username}
                        onChange={(e) => setUsername(e.target.value)}
                        placeholder="Username must be at least 6 characters"
                        className="form-control"
                      />
                      <label className="signup-label">
                        Phone number <span>(*)</span>
                      </label>
                      <input
                        type="text"
                        value={phoneNumber}
                        onChange={(e) => setPhoneNumber(e.target.value)}
                        placeholder="E.x: +84123456789"
                        className="form-control"
                      />
                      <label className="signup-label">
                        Password <span>(*)</span>
                      </label>
                      <div className="password-control">
                        <input
                          type={showPassword ? "text" : "password"}
                          value={password}
                          onChange={(e) => setPassword(e.target.value)}
                          placeholder="Password must be at least 6 characters"
                          className="form-control"
                        />

                        <div onClick={() => setShowPassword(!showPassword)}>
                          {showPassword ? (
                            <FaEye
                              className="fa fa-eye password-icon"
                              aria-hidden="true"
                            ></FaEye>
                          ) : (
                            <FaEyeSlash
                              className="fa fa-eye password-icon"
                              aria-hidden="true"
                            ></FaEyeSlash>
                          )}
                        </div>
                      </div>
                      <label className="signup-label">
                        Confirm Password <span>(*)</span>
                      </label>
                      <div className="password-control">
                        <input
                          type={showConfirmPassword ? "text" : "password"}
                          value={confirmPassword}
                          onChange={(e) => setConfirmPassword(e.target.value)}
                          placeholder="Confirm password"
                          className="form-control"
                        />

                        <div
                          onClick={() =>
                            setShowConfirmPassword(!showConfirmPassword)
                          }
                        >
                          {showConfirmPassword ? (
                            <FaEye
                              className="fa fa-eye password-icon"
                              aria-hidden="true"
                            ></FaEye>
                          ) : (
                            <FaEyeSlash
                              className="fa fa-eye password-icon"
                              aria-hidden="true"
                            ></FaEyeSlash>
                          )}
                        </div>
                      </div>

                      <select
                        className="form-control form-select"
                        aria-label="Default select example"
                        onChange={(e) => setRole(e.target.value)}
                      >
                        <option>
                          Select your role <span>(*)</span>
                        </option>
                        <option value="employee">Employee</option>
                        <option value="employer">Employer</option>
                      </select>
                    </div>
                    <div className="input-group go-home">
                      <div></div>
                      <Link style={{ fontSize: "20px" }} to="/">
                        {"<<"} Go Home
                      </Link>
                    </div>
                    <div className="form-group text-center">
                      <button
                        type="submit"
                        className="btn btn-primary"
                        onClick={handleSubmit}
                      >
                        Sign Up
                      </button>
                      <p className="mb-0">
                        <Link to="/login">Having an account? Login</Link>
                      </p>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default SignUp;
