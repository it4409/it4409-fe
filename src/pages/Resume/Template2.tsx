import { useEffect } from "react";
import { ITemplateProps } from "./Resume";
import "./Template2.scss";
import { formatDateToMMYYYY } from "../../utils";

function Template2(props: ITemplateProps) {
  const { formData } = props;

  useEffect(() => {
    const skillsProgList = document.querySelectorAll(".skills-prog li");

    skillsProgList.forEach((li, i) => {
      const skillsBar = li.querySelector(".skills-bar");
      const bar = skillsBar?.querySelector(".bar") as HTMLElement;
      const percent = parseInt(li.getAttribute("data-percent") || "0");

      setTimeout(() => {
        bar.style.width = percent + "%";
        bar.style.transitionDuration = ".5s";
      }, i * 150);
    });
  }, [formData.skill]);

  return (
    <>
      <link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css"
      />

      <div className="resume2">
        <div className="base">
          <div className="profile">
            <div className="photo">
              <img src={`${formData.avatar}`} />
            </div>
            <div className="info">
              <h2 className="name">{formData.name}</h2>
              {formData.dateOfBirth && (
                <p className="date-of-birth">{formData.dateOfBirth}</p>
              )}
              {formData.gender && <p className="gender">{formData.gender}</p>}
            </div>
          </div>
          <div className="contact">
            <h3>Contact Me</h3>
            <div className="call">
              <a href={`tel:${formData.mobilePhone}`}>
                <i className="fas fa-phone"></i>
                <span>{formData.mobilePhone}</span>
              </a>
            </div>
            <div className="address">
              <a href="">
                <i className="fas fa-map-marker"></i>
                <span>{formData.location}</span>
              </a>
            </div>
            <div className="email">
              <a href="">
                <i className="fas fa-envelope"></i>
                <span>{formData.email}</span>
              </a>
            </div>
            <div className="website">
              <a href={formData.gitHubLink} target="_blank" rel="noreferrer">
                {" "}
                <i className="fab fa-github"></i>
                <span>{formData.gitHubLink}</span>
              </a>
            </div>
            <div className="website">
              <a href={formData.linkedInLink} target="_blank" rel="noreferrer">
                {" "}
                <i className="fab fa-linkedin"></i>
                <span>{formData.linkedInLink}</span>
              </a>
            </div>
          </div>
        </div>
        <div className="func">
          <hr />
          <div className="work">
            <h3>
              <i className="fa fa-briefcase"></i>Experience
            </h3>
            <ul>
              {formData?.workingExperience?.map((work, index) => (
                <li>
                  <span>{work.companyName}</span>
                  <small>{work.position}</small>
                  <pre>{work.responsibility}</pre>
                  <small>
                    {work.from && formatDateToMMYYYY(work.from)}
                    {work.from && work.to ? "-" : ""}
                    {work.to && formatDateToMMYYYY(work.to)}{" "}
                  </small>
                </li>
              ))}
            </ul>
          </div>
          <hr />
          <div className="edu">
            <h3>
              <i className="fa fa-graduation-cap"></i>Education
            </h3>
            <ul>
              {formData?.education?.map((edu, index) => (
                <li>
                  <span>{edu.schoolName}</span>
                  <small>{edu.major}</small>
                  <small>
                    {edu.from && formatDateToMMYYYY(edu.from)}
                    {edu.from && edu.to ? "-" : ""}
                    {edu.to && formatDateToMMYYYY(edu.to)}
                  </small>
                </li>
              ))}
            </ul>
          </div>
          <hr />
          <div className="skills-prog">
            <h3>
              <i className="fas fa-code"></i>Programming Skills
            </h3>
            <ul>
              {formData?.skill?.map((item, index: number) => (
                <li data-percent="100" key={index}>
                  <span>{item.title}</span>
                  <div className="skills-bar">
                    <div className="bar"></div>
                  </div>
                </li>
              ))}
            </ul>
          </div>
          <hr />
          <div className="interests">
            <h3>
              <i className="fas fa-star"></i>Interests
            </h3>
            <div className="interests-items">
              <div className="art">
                <i className="fas fa-palette"></i>
                <span>Art</span>
              </div>
              <div className="art">
                <i className="fas fa-book"></i>
                <span>Books</span>
              </div>
              <div className="movies">
                <i className="fas fa-film"></i>
                <span>Movies</span>
              </div>
              <div className="music">
                <i className="fas fa-headphones"></i>
                <span>Music</span>
              </div>
              <div className="games">
                <i className="fas fa-gamepad"></i>
                <span>Games</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Template2;
