import React, { useEffect, useRef, useState } from "react";
import html2canvas from "html2canvas";
import jsPDF from "jspdf";
import Template1 from "./Template1";
import Template2 from "./Template2";
import "./Resume.scss";
import {
  Education,
  Employee,
  Skill,
  WorkingExperience,
} from "../../ultils/type";
import { useNavigate, useParams } from "react-router";
import { useAppDispatch, useAppSelector } from "../../redux/hooks";
import {
  getEmployeeProfile,
  updateEmployeeResume,
  uploadEmployeeResumePlus,
} from "../Profile/EmployeeApi";
import { handleFile } from "../../services/handleFile";
import { toast } from "react-toastify";
import { Button, Form, Modal } from "react-bootstrap";
import { convertMmToPixel, formatDateToYYYYMMDD } from "../../utils";


export interface ITemplateProps {
  formData: Employee;
}

const Resume = () => {
  // check if user is logged in and user is employee

  // if not, redirect to login page
  const [uploading, setUploading] = useState(false);
  const [resumeTitle, setResumeTitle] = useState(
    "Resume created at " + new Date().toLocaleString()
  );

  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const currentUser = useAppSelector((store) => store.login.userInfo);
  const { id = currentUser.id, cvId = null } = useParams();
  const editCv = useAppSelector((store) =>
    cvId ? store.profile.myCV.filter((cv) => cv.id === cvId)[0] : null
  );

  if (!currentUser || currentUser.role !== "employee") {
    navigate("/login");
  }

  useEffect(() => {
    if (!currentUser || currentUser.role !== "employee") {
      navigate("/");
    }
    dispatch(getEmployeeProfile({ id: id }));
  }, []);

  const profileEmployee = useAppSelector(
    (store) => store.profile.employeeProfile
  );

  const [template, setTemplate] = useState(1); // 1, 2
  // const [formData, setFormData] = useState<Employee>({} as Employee);
  const [formData, setFormData] = useState<Employee>({
    id: "",
    userId: "",
    email: "",
    name: "",
    avatar: "",
    gender: "",
    dateOfBirth: "",
    location: "",
    mobilePhone: "",
    linkedInLink: "",
    gitHubLink: "",
    created_at: "",
    updated_at: "",
    education: [],
    workingExperience: [],
    skill: [],
  });

  const [newEdu, setNewEdu] = useState<Education>({} as Education);
  const [newWorkExp, setNewWorkExp] = useState<WorkingExperience>(
    {} as WorkingExperience
  );
  const [newSkill, setNewSkill] = useState<Skill>({} as Skill);

  const [selectedEducation, setSelectedEducation] = useState<Education[]>([]);
  const [selectedWorkingExperience, setSelectedWorkingExperience] = useState<
    WorkingExperience[]
  >([]);
  const [selectedSkills, setSelectedSkills] = useState<Skill[]>([]);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };

  const handleSkillSelect = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { value, checked } = e.target;
    const skill = skillList.find((skill) => skill.title === value);
    if (!skill) return;
    if (checked) {
      setSelectedSkills((prevSelectedSkills) => [...prevSelectedSkills, skill]);
    } else {
      setSelectedSkills((prevSelectedSkills) =>
        prevSelectedSkills.filter((skill) => skill.title !== value)
      );
    }
  };

  const handleEducationSelect = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { value, checked } = e.target;
    const edu = educationList.find(
      (education) => education.schoolName === value
    );
    if (!edu) return;
    if (checked) {
      setSelectedEducation((prevSelectedEducation) => [
        ...prevSelectedEducation,
        edu,
      ]);
    } else {
      setSelectedEducation((prevSelectedEducation) =>
        prevSelectedEducation.filter(
          (education) => education.schoolName !== value
        )
      );
    }
  };

  const handleWorkingExperienceSelect = (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    const { value, checked } = e.target;
    const workExp = workingExperienceList.find(
      (experience) => experience.companyName === value
    );
    if (!workExp) return;
    if (checked) {
      setSelectedWorkingExperience((prevSelectedWorkingExperience) => [
        ...prevSelectedWorkingExperience,
        workExp,
      ]);
    } else {
      setSelectedWorkingExperience((prevSelectedWorkingExperience) =>
        prevSelectedWorkingExperience.filter(
          (experience) => experience.companyName !== value
        )
      );
    }
  };

  const divToPrintRef = useRef<HTMLDivElement>(null);

  const printDocument = async () => {
    const input = divToPrintRef.current;
    if (input) {
      try {
        const canvas = await html2canvas(input);
        const doc = new jsPDF({
          orientation: "p",
          unit: "px",
          format: [convertMmToPixel(220), canvas.height],
        });
        const imgData = canvas.toDataURL("image/png");
        doc.addImage(
          imgData,
          "PNG",
          0,
          0,
          convertMmToPixel(220),
          canvas.height
        );

        const pdfBlob = doc.output("blob"); // Generate the PDF blob
        const pdfFile = new File([pdfBlob], "resume.pdf", {
          type: "application/pdf",
        });
        if (cvId) {
          await handleUpdateCV(pdfFile);
        } else {
          await handleUploadCV(pdfFile);
        }
        navigate(`/info/${id}`);
      } catch (error) {
        console.error("Error generating PDF:", error);
      }
    }
  };

  const handleUpdateCV = async (file: File) => {
    setUploading(true);
    await dispatch(handleFile(file)).then((res) => {
      if (res.payload && cvId) {
        dispatch(
          updateEmployeeResume({
            id: cvId,
            title: resumeTitle,
            link: res.payload,
            email: formData.email,
            name: formData.name,
            gender: formData.gender,
            dateOfBirth: formData.dateOfBirth,
            location: formData.location,
            linkedInLink: formData.linkedInLink,
            gitHubLink: formData.gitHubLink,
            mobilePhone: formData.mobilePhone.startsWith("+84")
              ? formData.mobilePhone
              : formData.mobilePhone.startsWith("0")
              ? `+84${formData.mobilePhone.slice(1)}`
              : "",
            skills: selectedSkills,
            educations: selectedEducation,
            workingExperiences: selectedWorkingExperience,
            method: "create",
          })
        )
          .then((resObj) => {
            toast.success("upload resume successfully");
          })
          .catch((err) => {
            toast.error(String(err));
          })
          .finally(() => {
            setUploading(false); // Set the loading state to false after the API call
          });
      }
    });
  };

  const handleUploadCV = async (file: File) => {
    setUploading(true);
    await dispatch(handleFile(file)).then((res) => {
      if (res.payload) {
        dispatch(
          uploadEmployeeResumePlus({
            title: resumeTitle,
            link: res.payload,
            email: formData.email,
            name: formData.name,
            gender: formData.gender,
            dateOfBirth: formData.dateOfBirth,
            location: formData.location,
            linkedInLink: formData.linkedInLink,
            gitHubLink: formData.gitHubLink,
            mobilePhone: formData.mobilePhone.startsWith("+84")
              ? formData.mobilePhone
              : formData.mobilePhone.startsWith("0")
              ? `+84${formData.mobilePhone.slice(1)}`
              : "",
            skills: selectedSkills,
            educations: selectedEducation,
            workingExperiences: selectedWorkingExperience,
            method: "create",
          })
        )
          .then((resObj) => {
            toast.success("upload resume successfully");
          })
          .catch((err) => {
            toast.error(String(err));
          })
          .finally(() => {
            setUploading(false); // Set the loading state to false after the API call
          });
      }
    });
  };

  // const educationList = profileEmployee.education;
  // const workingExperienceList = profileEmployee.workingExperience;
  // const skillList = profileEmployee.skill;

  const [educationList, setEducationList] = useState<Education[]>(
    profileEmployee.education
  );
  const [workingExperienceList, setWorkingExperienceList] = useState<
    WorkingExperience[]
  >(profileEmployee.workingExperience);
  const [skillList, setSkillList] = useState<Skill[]>(profileEmployee.skill);

  const handleAddEducation = () => {
    setEducationList((prevEducationList) => [...prevEducationList, newEdu]);
    setNewEdu({} as Education);
    setShowAddEducation(false);
  };

  const handleAddSkill = () => {
    setSkillList((prevSkillList) => [...prevSkillList, newSkill]);
    setNewSkill({} as Skill);
    setShowAddSkill(false);
  };

  const handleAddWorkingExperience = () => {
    setWorkingExperienceList((prevWorkingExperienceList) => [
      ...prevWorkingExperienceList,
      newWorkExp,
    ]);
    setNewWorkExp({} as WorkingExperience);
    setShowAddWorkingExperience(false);
  };

  const [showAddSkill, setShowAddSkill] = useState(false);
  const [showAddEducation, setShowAddEducation] = useState(false);
  const [showAddWorkingExperience, setShowAddWorkingExperience] =
    useState(false);

  useEffect(() => {
    setSelectedEducation(
      profileEmployee.education?.length
        ? profileEmployee.education
        : educationList
    );
    setSelectedWorkingExperience(
      profileEmployee.workingExperience?.length
        ? profileEmployee.workingExperience
        : workingExperienceList
    );
    setSelectedSkills(
      profileEmployee.skill?.length ? profileEmployee.skill : skillList
    );
    if (editCv) {
      setFormData({
        ...profileEmployee,
        mobilePhone: editCv.mobilePhone,
        email: editCv.email,
        name: editCv.name,
        gender: editCv.gender,
        dateOfBirth: formatDateToYYYYMMDD(editCv.dateOfBirth),
        location: editCv.location,
        linkedInLink: editCv.linkedInLink,
        gitHubLink: editCv.gitHubLink,
        education: editCv.educations,
        workingExperience: editCv.workingExperiences,
        skill: editCv.skills,
      });
      setResumeTitle(editCv.title);
      setSkillList(editCv.skills);
      setEducationList(editCv.educations);
      setWorkingExperienceList(editCv.workingExperiences);
      setSelectedEducation(editCv.educations);
      setSelectedWorkingExperience(editCv.workingExperiences);
      setSelectedSkills(editCv.skills);
    } else setFormData({ ...profileEmployee });
  }, [profileEmployee, cvId]);

  useEffect(() => {
    setFormData((prevFormData) => ({
      ...prevFormData,
      education: selectedEducation,
    }));
  }, [selectedEducation]);

  useEffect(() => {
    setFormData((prevFormData) => ({
      ...prevFormData,
      workingExperience: selectedWorkingExperience,
    }));
  }, [selectedWorkingExperience]);

  useEffect(() => {
    setFormData((prevFormData) => ({
      ...prevFormData,
      skill: selectedSkills,
    }));
  }, [selectedSkills]);

  return !Object.keys(profileEmployee).length ? (
    <div>Waiting ...</div>
  ) : (
    <div
      className="resume-container"
      style={{ maxWidth: "80%", margin: "auto" }}
    >
      {uploading && <div className="loading-indicator">Uploading...</div>}

      <div className="row">
        <div className="col-8">
          <div id="divToPrint" className="mt4" ref={divToPrintRef}>
            {template === 1 && <Template1 formData={formData} />}
            {template === 2 && <Template2 formData={formData} />}
          </div>
        </div>
        <div className="col-4">
          <div
            className="right-side"
            style={{
              padding: "10px",
              backgroundColor: "#9fa8da",
              border: "1px solid #f4fcfc",
              boxShadow: "1px 1px 1px #000",
            }}
          >
            <button
              className="btn btn-warning"
              onClick={printDocument}
              disabled={uploading}
              style={{ width: "100%" }}
            >
              Print
            </button>
            <>
              <label htmlFor="template">Select Template:</label>
              <select
                className="form-select form-select-sm"
                name="template"
                id="template"
                value={template}
                onChange={(e) => setTemplate(parseInt(e.target.value))}
              >
                <option value={1}>Template 1</option>
                <option value={2}>Template 2</option>
              </select>
              <br />
              <label htmlFor="title">Resume Title:</label>
              <input
                className="form-control"
                type="text"
                id="title"
                name="title"
                value={resumeTitle}
                onChange={(e) => setResumeTitle(e.target.value)}
              />
              <br />
              <label htmlFor="name">Name:</label>
              <input
                className="form-control"
                type="text"
                id="name"
                name="name"
                value={formData.name}
                onChange={handleChange}
              />
              <br />
              <label htmlFor="dateOfBirth">Date of birth:</label>
              <input
                className="form-control"
                type="date"
                id="dateOfBirth"
                name="dateOfBirth"
                value={formData.dateOfBirth}
                onChange={handleChange}
              />
              <br />
              {/* gender */}
              <label htmlFor="gender">Gender: </label>
              <select
                className="form-select form-select-sm"
                name="gender"
                id="gender"
                value={formData.gender}
                onChange={(e) => {
                  setFormData((prevFormData) => ({
                    ...prevFormData,
                    gender: e.target.value,
                  }));
                }}
              >
                <option value={"male"}>Male</option>
                <option value={"female"}>Female</option>
              </select>
              <br />
              <label htmlFor="email">Email:</label>
              <input
                className="form-control"
                type="email"
                id="email"
                name="email"
                value={formData.email}
                onChange={handleChange}
              />
              <br />
              <label htmlFor="mobilePhone">Phone:</label>
              <input
                className="form-control"
                type="text"
                id="mobilePhone"
                name="mobilePhone"
                value={formData.mobilePhone}
                onChange={handleChange}
              />
              <br />
              <label htmlFor="location">Address:</label>
              <input
                className="form-control"
                type="text"
                id="location"
                name="location"
                value={formData.location}
                onChange={handleChange}
              />
              <br />
              <label htmlFor="gitHubLink">Github:</label>
              <input
                className="form-control"
                type="text"
                id="gitHubLink"
                name="gitHubLink"
                value={formData.gitHubLink}
                onChange={handleChange}
              />
              <br />
              <label htmlFor="linkedInLink">Linkedin:</label>
              <input
                className="form-control"
                type="text"
                id="linkedInLink"
                name="linkedInLink"
                value={formData.linkedInLink}
                onChange={handleChange}
              />
              <br />
              <label>Select education:</label>
              <br />
              {educationList?.map((education) => (
                <div className="form-check">
                  <label
                    className="form-check-label"
                    key={education.schoolName}
                  >
                    <input
                      className="form-check-input"
                      type="checkbox"
                      value={education.schoolName}
                      onChange={handleEducationSelect}
                      checked={selectedEducation?.some(
                        (selected) =>
                          selected.schoolName === education.schoolName
                      )}
                    />
                    {education.schoolName}
                  </label>
                </div>
              ))}
              <button
                className="btn btn-small btn-primary"
                onClick={() => setShowAddEducation(true)}
              >
                Add Education
              </button>
              <br />
              <label>Select working experience:</label>
              <br />
              {workingExperienceList?.map((experience) => (
                <div className="form-check">
                  <label
                    className="form-check-label"
                    key={experience.companyName}
                  >
                    <input
                      className="form-check-input"
                      type="checkbox"
                      value={experience.companyName}
                      onChange={handleWorkingExperienceSelect}
                      checked={selectedWorkingExperience?.some(
                        (selected) =>
                          selected.companyName === experience.companyName
                      )}
                    />
                    {experience.companyName}
                  </label>
                </div>
              ))}
              <button
                className="btn btn-small btn-primary"
                onClick={() => setShowAddWorkingExperience(true)}
              >
                Add Working Exp
              </button>
              <br />
              <label>Select skills:</label> <br />
              {skillList?.map((skill) => (
                <div className="form-check" key={skill.title}>
                  <label className="form-check-label">
                    <input
                      className="form-check-input"
                      type="checkbox"
                      value={skill.title}
                      onChange={handleSkillSelect}
                      checked={selectedSkills?.some(
                        (selected) => selected.title === skill.title
                      )}
                    />
                    {skill.title}
                  </label>
                </div>
              ))}
              <button
                className="btn btn-small btn-primary"
                onClick={() => setShowAddSkill(true)}
              >
                Add Skill
              </button>
            </>
          </div>
        </div>
      </div>
      <Modal
        show={showAddSkill}
        onHide={() => setShowAddSkill(false)}
        size="sm"
      >
        <Modal.Header closeButton>
          <Modal.Title>Add To Skill List</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ paddingLeft: "30px" }}>
          <Form.Label>Title</Form.Label>
          <Form.Control
            placeholder="Enter title"
            onChange={(e) =>
              setNewSkill((prev) => {
                return {
                  ...prev,
                  title: e.target.value,
                };
              })
            }
          />
          <Form.Label>Certificate Link</Form.Label>
          <Form.Control
            placeholder="Enter certificate link"
            onChange={(e) =>
              setNewSkill((prev) => {
                return {
                  ...prev,
                  certificateLink: e.target.value,
                };
              })
            }
          />
          <Form.Label>Score</Form.Label>
          <Form.Control
            placeholder="Enter score"
            onChange={(e) =>
              setNewSkill((prev) => {
                return {
                  ...prev,
                  score: e.target.value,
                };
              })
            }
          />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowAddSkill(false)}>
            Close
          </Button>
          <Button variant="primary" onClick={() => handleAddSkill()}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
      <Modal
        show={showAddWorkingExperience}
        onHide={() => setShowAddWorkingExperience(false)}
        size="sm"
      >
        <Modal.Header closeButton>
          <Modal.Title>Add To Working Exp List</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ paddingLeft: "30px" }}>
          <Form.Label>Company name</Form.Label>
          <Form.Control
            placeholder="Enter company name"
            onChange={(e) =>
              setNewWorkExp((prev) => {
                return {
                  ...prev,
                  companyName: e.target.value,
                };
              })
            }
          />
          <Form.Label>Position</Form.Label>
          <Form.Control
            placeholder="Enter position"
            onChange={(e) =>
              setNewWorkExp((prev) => {
                return {
                  ...prev,
                  position: e.target.value,
                };
              })
            }
          />
          <Form.Label>Responsibility</Form.Label>
          <Form.Control
            placeholder="Enter responsibility"
            onChange={(e) =>
              setNewWorkExp((prev) => {
                return {
                  ...prev,
                  responsibility: e.target.value,
                };
              })
            }
          />
          <Form.Label>From</Form.Label>
          <input
            className="form-control"
            placeholder="Enter from date"
            type="date"
            onChange={(e) =>
              setNewWorkExp((prev) => {
                return {
                  ...prev,
                  from: e.target.value,
                };
              })
            }
          />
          <Form.Label>To</Form.Label>
          <input
            className="form-control"
            placeholder="Enter from date"
            type="date"
            onChange={(e) =>
              setNewWorkExp((prev) => {
                return {
                  ...prev,
                  to: e.target.value,
                };
              })
            }
          />
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={() => setShowAddWorkingExperience(false)}
          >
            Close
          </Button>
          <Button
            variant="primary"
            onClick={() => handleAddWorkingExperience()}
          >
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
      <Modal
        show={showAddEducation}
        onHide={() => setShowAddEducation(false)}
        size="sm"
      >
        <Modal.Header closeButton>
          <Modal.Title>Add To Education List</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ paddingLeft: "30px" }}>
          <Form.Label>School name</Form.Label>
          <Form.Control
            placeholder="Enter school name"
            onChange={(e) =>
              setNewEdu((prev) => {
                return {
                  ...prev,
                  schoolName: e.target.value,
                };
              })
            }
          />
          <Form.Label>Major</Form.Label>
          <Form.Control
            placeholder="Enter major"
            onChange={(e) =>
              setNewEdu((prev) => {
                return {
                  ...prev,
                  major: e.target.value,
                };
              })
            }
          />

          <Form.Label>From</Form.Label>
          <input
            className="form-control"
            placeholder="Enter from date"
            type="date"
            onChange={(e) =>
              setNewEdu((prev) => {
                return {
                  ...prev,
                  from: e.target.value,
                };
              })
            }
          />
          <Form.Label>To</Form.Label>
          <input
            className="form-control"
            placeholder="Enter from date"
            type="date"
            onChange={(e) =>
              setNewEdu((prev) => {
                return {
                  ...prev,
                  to: e.target.value,
                };
              })
            }
          />
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={() => setShowAddEducation(false)}
          >
            Close
          </Button>
          <Button variant="primary" onClick={() => handleAddEducation()}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default Resume;
