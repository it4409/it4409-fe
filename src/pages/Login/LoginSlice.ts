import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";

export interface LoginData {
  username: string;
  password: string;
}

export interface Token {
  accessToken: string;
  refreshToken: string;
}

export interface LoginResponse {
  token: Token;
  isLogin: boolean;
}

export interface UserInfo {
  email: string;
  id: string;
  mobilePhone: string;
  role: string;
  createdAt: string;
  updatedAt: string;
}

export interface LoginState {
  token: Token;
  isLogin: boolean;
  userInfo: UserInfo;
}

export const initialState: LoginState = {
  token: {
    accessToken: "",
    refreshToken: "",
  },
  isLogin: false,
  userInfo: {} as UserInfo,
};

export const LoginSlice = createSlice({
  name: "login",
  initialState,
  reducers: {
    setToken: (state, action: PayloadAction<LoginResponse>) => {
      state.token = action.payload.token;
      state.isLogin = action.payload.isLogin;
    },
    setLogin: (state, action: PayloadAction<LoginState>) => {
      state.token = action.payload.token;
      state.isLogin = action.payload.isLogin;
      state.userInfo = action.payload.userInfo;
    },
  },
});

export const { setToken, setLogin } = LoginSlice.actions;
export default LoginSlice.reducer;
