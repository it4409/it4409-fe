import React, { useEffect, useState } from "react";
import "./Login.scss";
import "bootstrap/dist/css/bootstrap.min.css";
import { Link, useNavigate } from "react-router-dom";
import { FaEye, FaEyeSlash } from "react-icons/fa";
import { toast } from "react-toastify";
import { useAppDispatch, useAppSelector } from "../../redux/hooks";
import { getUserInfo, loginAPI } from "../../services/authenService";
import { setLogin, setToken } from "./LoginSlice";
import axiosWithAuth from "../../axiosInstance";

const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const validate = () => {
    if (!username || !password) {
      toast.error("Please fill in all fields");
      return false;
    }
    return true;
  };

  const handleSubmit = async (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    if (!validate()) return;
    try {
      const res = await loginAPI(username, password);
      if (res.status === 201) {
        dispatch(
          setToken({
            token: {
              accessToken: res.data.accessToken,
              refreshToken: res.data.refreshToken,
            },
            isLogin: true,
          })
        );
        navigate("/");
        toast.success("Login successfully");
      } else {
        toast.error(res.data.message);
      }
    } catch (error: any) {
      toast.error(error.response.data.message);
    }
  };

  return (
    <>
      <div className="container-fluid login-1">
        <div className="container">
          <div className="login-form">
            <div className="row align-items-center">
              <div className="col-md-6">
                <div className="img-box">
                  <img
                    src="./images/login-img.png"
                    className="back-img"
                    title="login"
                    alt="welcome"
                  />
                </div>
              </div>
              <div className="col-md-6">
                <div className="login-box">
                  <form>
                    <div className="form-group">
                      <h4 className="login-title">Welcome back</h4>
                      <p className="login-p">Please login to your account.</p>
                    </div>
                    <div className="form-group">
                      <input
                        type="text"
                        value={username}
                        onChange={(e) => setUsername(e.target.value)}
                        placeholder="Enter your name"
                        className="form-control"
                      />
                      <div className="password-control">
                        <input
                          type={showPassword ? "text" : "password"}
                          value={password}
                          onChange={(e) => setPassword(e.target.value)}
                          placeholder="Enter your password"
                          className="form-control"
                        />

                        <div onClick={() => setShowPassword(!showPassword)}>
                          {showPassword ? (
                            <FaEye
                              className="fa fa-eye password-icon"
                              aria-hidden="true"
                            ></FaEye>
                          ) : (
                            <FaEyeSlash
                              className="fa fa-eye password-icon"
                              aria-hidden="true"
                            ></FaEyeSlash>
                          )}
                        </div>
                      </div>
                    </div>
                    <div className="input-group go-home">
                      <Link to="#" className="forgot">
                        Forgot password?
                      </Link>
                      <Link style={{ fontSize: "20px" }} to="/">
                        {"<<"} Go Home
                      </Link>
                    </div>
                    <div className="form-group text-center">
                      <button
                        type="submit"
                        className="btn btn-primary"
                        onClick={handleSubmit}
                      >
                        Login
                      </button>
                      <p className="mb-0">
                        <Link to="/signup">New User? Sign Up</Link>
                      </p>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Login;
