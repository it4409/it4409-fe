import React, { useState, useEffect, memo } from "react";
import Footer from "../../components/Footer/Footer";
import Navbar from "../../components/Navbar/Navbar";
import { useAppDispatch, useAppSelector } from "../../redux/hooks";
import { Company } from "../../ultils/type";
import { searchJob } from "../Jobs/JobsApi";
import { setSearchConditions } from "../Jobs/JobSlice";
import { useNavigate } from "react-router-dom";
import { getEmployerProfileView } from "../Profile/ProfileView/ProfileViewApi";

const CompanyList = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const compaiesFetch = useAppSelector((store) => store.company.companyList);

  const [companyList, setCompanyList] = useState<Company[]>([]);
  const [textSearch, setTextSearch] = useState<string>("");

  const [isScrolled, setIsScrolled] = useState(true);
  window.onscroll = () => {
    setIsScrolled(window.scrollY === 0 ? true : false);
    return () => (window.onscroll = null);
  };

  useEffect(() => {
    setCompanyList(compaiesFetch);
  }, [compaiesFetch]);

  const filterList = companyList.filter((item) =>
    item.name.toLowerCase().includes(textSearch.toLowerCase())
  );

  const getJobsByCompany = async (company: Company) => {
    await dispatch(searchJob({ companyId: company.id }));
    dispatch(setSearchConditions([company.name]));
    navigate("/result");
  };

  return (
    <div className="bg-dark" style={{ paddingTop: "7.5rem" }}>
      <Navbar isScrolled={isScrolled} />
      <div className="container content min-vh-100">
        <form>
          <div className="form-group pb-2">
            <label className="text-light">Company name</label>
            <input
              type="text"
              className="form-control"
              placeholder="Keyword"
              value={textSearch}
              onChange={(e) => setTextSearch(e.target.value)}
            />
          </div>
        </form>
        <div
          className="companyList p-2"
          style={{
            display: "grid",
            gridTemplateColumns: "1fr 1fr 1fr 1fr",
            maxHeight: "60rem",
            overflow: "auto",
          }}
        >
          {filterList?.map((item) => (
            <div className="card mb-4" style={{ width: "18rem" }} key={item.id}>
              <img
                style={{ maxHeight: "35vh", minHeight: "23vh" }}
                src={item.logoLink}
                alt="Card"
              />
              <div className="card-body">
                <h5 className="card-title">{item.name}</h5>
                <div
                  className="desc"
                  style={{
                    maxHeight: "10rem",
                    overflow: "hidden",
                    textOverflow: "ellipsis",
                    whiteSpace: "nowrap",
                  }}
                >
                  <pre className="card-text">{item.description}</pre>
                </div>
                <div className="flex" style={{ flexWrap: "wrap" }}>
                  <button
                    className="btn btn-primary m-1"
                    onClick={async () => {
                      await dispatch(
                        getEmployerProfileView({ id: item.userId })
                      );
                      navigate(`/profile-view-company/${item.userId}`);
                    }}
                  >
                    About Company
                  </button>
                  <button
                    className="btn btn-secondary m-1"
                    onClick={() => getJobsByCompany(item)}
                  >
                    See Jobs
                  </button>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default memo(CompanyList);
