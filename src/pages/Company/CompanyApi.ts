import { createAsyncThunk } from "@reduxjs/toolkit";
import { BASE_URL } from "../../ultils/constant";
import axios from "axios";
import { Company } from "../../ultils/type";

const getCompanyList = createAsyncThunk<Company[], void>(
    "/get-companies",
    async () => {
        const res = (await axios.get(`${BASE_URL}/employer`)).data
        return res
    }
);

export { getCompanyList } 