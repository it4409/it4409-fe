import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from '@reduxjs/toolkit'
import { RootState } from "../../redux/store";
import { getCompanyList } from "./CompanyApi";
import { Company } from "../../ultils/type";
export interface CompanyState{
  companyList: Company[],
  companyListFetched: boolean,
}

const initialState: CompanyState = {
    companyList: [],
    companyListFetched: false
}

export const CompanySlice = createSlice({
  name: "Company",
  initialState,
  reducers: {
    setCompanyList:(state, action: PayloadAction<Company[]>) =>{
      state.companyList = action.payload;
    },
    addCompany: (state, action: PayloadAction<Company>) => {
      state.companyList.push(action.payload)
    },
  },
  extraReducers:(builder) => {
    builder.addCase(getCompanyList.fulfilled,(state,action)=> {
      state.companyList = action.payload
    })
  }
});

export const { setCompanyList ,addCompany } = CompanySlice.actions;
export const showCompanyList = (state: RootState) => state.company
export default CompanySlice.reducer;