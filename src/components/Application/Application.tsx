import React, { memo, useEffect, useState } from "react";
import { Modal, Form } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { getEmployeeProfileView } from "../../pages/Profile/ProfileView/ProfileViewApi";
import { useAppDispatch } from "../../redux/hooks";
import { Application, EApplicationStatus } from "../../ultils/type";
import { formatTime } from "../../utils";

interface ApplicationProps {
  show: boolean;
  onHide: () => void;
  modalTitle: string;
  jobPostId?: string;
  applicationList: Application[];
  isOwner: boolean;
  handleReply?: (appId: string, replyStatus: string) => void;
  filterApplication?: (
    jobId: string,
    filter: {
      skillTitle?: string;
      yearOfWorkingExperience?: number;
      schoolName?: string;
    }
  ) => void;
}

const checkList = [
  EApplicationStatus.pending,
  EApplicationStatus.accepted,
  EApplicationStatus.rejected,
];

const ApplicationList = (props: ApplicationProps) => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const [applyList, setApplyList] = useState<Application[]>([]);
  const [filters, setFilters] = useState<{
    skillTitle?: string;
    yearOfWorkingExperience?: number;
    schoolName?: string;
  }>({});

  const [filterStatus, setfilterStatus] = useState([
    "pending",
    "accepted",
    "rejected",
  ]);

  useEffect(() => {
    setApplyList(props.applicationList);
  }, [props.applicationList]);

  useEffect(() => {
    setApplyList(
      props.applicationList.filter((app) => filterStatus.includes(app.status))
    );
  }, [filterStatus, props.applicationList]);

  const renderBadgeStatus = (status: EApplicationStatus) => {
    switch (status) {
      case "pending":
        return (
          <span className={`badge badge-pill bg-opacity-80 bg-info`}>
            {status}
          </span>
        );

      case "accepted":
        return (
          <span className={`badge badge-pill bg-opacity-80 bg-success`}>
            {status}
          </span>
        );

      case "rejected":
        return (
          <span className={`badge badge-pill bg-opacity-80 bg-danger`}>
            {status}
          </span>
        );

      default:
        return;
    }
  };

  const handleFilterApplication = () => {
    props.filterApplication &&
      props.jobPostId &&
      props.filterApplication(props.jobPostId, filters);
  };

  const handleFilterStatus = (status: string) => {
    if (filterStatus.includes(status)) {
      setfilterStatus((prev) => prev.filter((item) => item !== status));
    } else setfilterStatus((prev) => [...prev, status]);
  };

  return (
    <>
      <Modal show={props.show} onHide={() => props.onHide()} size="xl">
        <Modal.Header closeButton>
          <Modal.Title>{props.modalTitle}</Modal.Title>
        </Modal.Header>
        <Modal.Body
          style={{ paddingLeft: "20px", maxHeight: "80rem", overflow: "auto" }}
        >
          {props.isOwner && props.jobPostId && (
            <div>
              <a href={`/job-detail/${props.jobPostId}`}>Go to job Post</a>
              <div className="flex my-3">
                <Form.Control
                  className="m-1"
                  type="text"
                  placeholder="Skill"
                  onChange={(e) =>
                    setFilters((prev) => {
                      return {
                        ...prev,
                        skillTitle: e.target.value,
                      };
                    })
                  }
                />
                <Form.Control
                  className="m-1"
                  type="number"
                  placeholder="Experience"
                  onChange={(e) =>
                    setFilters((prev) => {
                      return {
                        ...prev,
                        yearOfWorkingExperience: Number(e.target.value),
                      };
                    })
                  }
                />
                <Form.Control
                  className="m-1"
                  type="text"
                  placeholder="School"
                  onChange={(e) =>
                    setFilters((prev) => {
                      return {
                        ...prev,
                        schoolName: e.target.value,
                      };
                    })
                  }
                />
                <button
                  className="btn btn-primary"
                  onClick={handleFilterApplication}
                >
                  Filter
                </button>
              </div>
            </div>
          )}
          <div className="flex j-between w-25">
            {checkList.map((stt) => {
              return (
                <div className="form-check" key={stt}>
                  <input
                    className="form-check-input"
                    type="checkbox"
                    value={stt}
                    checked={filterStatus.includes(stt)}
                    onChange={(e) => handleFilterStatus(e.target.value)}
                  />
                  <label className="form-check-label">
                    {renderBadgeStatus(stt)}
                  </label>
                </div>
              );
            })}
          </div>
          {applyList.length === 0 ? (
            <div className="flex j-center text-truncate text-muted p-4">
              There is no applications available
            </div>
          ) : (
            <table className="table">
              <thead>
                <tr>
                  <th scope="col">Resume</th>
                  <th scope="col">Email</th>
                  <th scope="col">Phone</th>
                  <th scope="col">Submit at</th>
                  <th scope="col">Status</th>
                  {props.isOwner ? (
                    <th scope="col"></th>
                  ) : (
                    <th scope="col">Job Post</th>
                  )}
                </tr>
              </thead>
              <tbody>
                {applyList.map((app, index) => (
                  <tr key={index}>
                    <td>
                      <a href={app.resume?.link}>{app.resume?.title}</a>
                    </td>
                    <td>{app.employee.email}</td>
                    <td>{app.employee.mobilePhone}</td>
                    <td>{formatTime(app.createdAt)}</td>
                    <td>{renderBadgeStatus(app.status)}</td>
                    {props.isOwner ? (
                      <td>
                        {app.status === "pending" && (
                          <div className="btn-group flex">
                            <button
                              className="btn btn-info btn-sm rounded me-1"
                              onClick={async () => {
                                await dispatch(
                                  getEmployeeProfileView({
                                    id: app.employee.userId,
                                  })
                                );
                                navigate(
                                  `/profile-view-employee/${app.employee.userId}`
                                );
                              }}
                            >
                              See Profile
                            </button>
                            <button
                              type="button"
                              className="btn btn-danger dropdown-toggle btn-sm rounded"
                              data-bs-toggle="dropdown"
                              aria-expanded="false"
                            >
                              Reply
                            </button>
                            <ul className="dropdown-menu">
                              <li>
                                <button
                                  className="dropdown-item"
                                  onClick={() =>
                                    props.handleReply &&
                                    props.handleReply(app.id, "accepted")
                                  }
                                >
                                  Accept
                                </button>
                              </li>
                              <li>
                                <button
                                  className="dropdown-item"
                                  onClick={() =>
                                    props.handleReply &&
                                    props.handleReply(app.id, "rejected")
                                  }
                                >
                                  Reject
                                </button>
                              </li>
                            </ul>
                          </div>
                        )}
                      </td>
                    ) : (
                      <td>
                        <a href={`/job-detail/${app.jobPostId}`}>
                          {app.job?.title}
                        </a>
                      </td>
                    )}
                  </tr>
                ))}
              </tbody>
            </table>
          )}
        </Modal.Body>
      </Modal>
    </>
  );
};

export default memo(ApplicationList);
