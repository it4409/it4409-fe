import { memo, useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import "./Navbar.scss";
import { FaPowerOff } from "react-icons/fa";
import { useAppDispatch, useAppSelector } from "../../redux/hooks";
import { Button } from "react-bootstrap";
import {
  initialState as logoutState,
  setLogin,
} from "../../pages/Login/LoginSlice";
import { getUserInfo } from "../../services/authenService";
import { User } from "../../ultils/type";

interface NavbarProps {
  isScrolled?: boolean;
}

const Navbar = (props: NavbarProps) => {
  const [isLogin, setIsLogin] = useState(false);
  const [userInfo, setUserInfo] = useState({} as User);
  const loginState = useAppSelector((state) => state.login);
  const links = [
    { name: "Home", link: "/" },
    { name: "Jobs", link: "/jobs" },
    { name: "Company", link: "/company-list" },
    loginState.isLogin
      ? { name: "My Information", link: `/info/${userInfo.id}` }
      : { name: "", link: "" },
  ];

  const navigate = useNavigate();
  const dispatch = useAppDispatch();

  const handleLogout = () => {
    dispatch(setLogin(logoutState));
    navigate("/login");
  };

  const getIsLogin = () => {
    const persistData = localStorage.getItem("persist:root");
    if (persistData) {
      const loginData = JSON.parse(JSON.parse(persistData).login);
      return loginData.isLogin;
    }
  };

  const handleIsLogin = async () => {
    const userInfo = await getUserInfo();
    dispatch(
      setLogin({
        ...loginState,
        userInfo,
      })
    );
    setUserInfo(userInfo);
  };

  useEffect(() => {
    setIsLogin(getIsLogin());
    if (isLogin) handleIsLogin();
    // eslint-disable-next-line
  }, [isLogin]);

  return (
    <div>
      <nav className={`flex ${props.isScrolled ? "" : "scrolled"}`}>
        <div className="left flex a-center">
          <ul className="links flex">
            {links.map(({ name, link }) => {
              return (
                <li key={name}>
                  <Link to={link}>{name}</Link>
                </li>
              );
            })}
          </ul>
        </div>
        <div className="right flex a-center">
          {isLogin ? (
            <>
              <span style={{ color: "white" }}>Hello, {userInfo?.email}</span>
              <button onClick={handleLogout}>
                <FaPowerOff />
              </button>
            </>
          ) : (
            <Button onClick={() => navigate("/login")}>Login</Button>
          )}
        </div>
      </nav>
    </div>
  );
};

export default memo(Navbar);
