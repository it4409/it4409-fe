import { combineReducers, configureStore } from "@reduxjs/toolkit";
import JobSlice from "../pages/Jobs/JobSlice";
import CompanySlice from "../pages/Company/CompanySlice";
import LoginSlice from "../pages/Login/LoginSlice";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import ProfileSlice from "../pages/Profile/ProfileSlice";
import ProfileViewSlice from "../pages/Profile/ProfileView/ProfileViewSlice";

const rootReducer = combineReducers({
  login: LoginSlice,
  job: JobSlice,
  company: CompanySlice,
  profile: ProfileSlice,
  profileView: ProfileViewSlice
});

const persistConfig = {
  key: "root",
  storage: storage,
  whitelist: ["login"],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = configureStore({
  reducer: persistedReducer,
});

export const persistor = persistStore(store);

export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;
