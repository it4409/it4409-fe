export enum EROLENAME {
  employee = "EMPLOYEE",
  company = "COMPANY",
}

export interface User {
  id: string;
  username: string;
  email: string;
  mobilePhone: string;
  password: string;
  role: EROLENAME;
  created_at: string;
  updated_at: string;
}

export interface Employee {
  id: string;
  userId: string;
  email: string;
  name: string;
  avatar: string;
  gender: string;
  dateOfBirth: string;
  location: string;
  mobilePhone: string;
  linkedInLink: string;
  gitHubLink: string;
  created_at: string;
  updated_at: string;
  education: Education[];
  workingExperience: WorkingExperience[];
  skill: Skill[];
}

export interface Company {
  id: string;
  userId: string;
  email: string;
  name: string;
  logoLink: string;
  locations: string[];
  mobilePhone: string[];
  websiteLink: string;
  description: string;
  created_at: string;
  updated_at: string;
}

export interface JobRequirement {
  title: string;
  id: string;
  score?: string;
}

export interface Job {
  id: string;
  title: string;
  active: boolean;
  companyId: string;
  description: string;
  locations: string[];
  keywords: JobRequirement[];
  requirement: string;
  salary: number;
  created_at: string;
  updated_at: string;
  companyLogo: string
}

export interface Education {
  schoolName: string;
  major: string;
  from?: string;
  to?: string;
  id: string;
}

export interface WorkingExperience {
  position: string;
  companyName: string;
  responsibility: string;
  from?: string;
  to?: string;
  id: string;
}

export interface Skill {
  title: string;
  certificateLink?: string;
  score?: string;
  id: string;
}

export interface Resume {
  title: string;
  employeeId: string;
  link: string;
  name: string;
  gender: string;
  dateOfBirth: string;
  location: string;
  linkedInLink: string;
  gitHubLink: string;
  email: string;
  mobilePhone: string;
  educations: Education[];
  workingExperiences: WorkingExperience[];
  skills: Skill[];
  id: string;
  createdAt: string;
  updatedAt: string;
  method: string;
}

export interface Application {
  employeeId: string;
  jobPostId: string;
  resumeId: string;
  status: EApplicationStatus;
  createdAt: string;
  updatedAt: string;
  id: string;
  resume: Resume;
  job: Job;
  employee: Employee;
  message: string;
}

export enum EApplicationStatus {
  pending = "pending",
  accepted = "accepted",
  rejected = "rejected",
}
