import { createAsyncThunk } from "@reduxjs/toolkit"
import axiosWithAuth from "../axiosInstance"
import { BASE_URL } from "../ultils/constant"

export const handleFile = createAsyncThunk(
    "/upload-file",
    async (file: File) => {
        const formData = new FormData();
        formData.append("file", file);
        const { data } = await axiosWithAuth.post(`${BASE_URL}/file/upload`, formData)
        return data
    }
)
