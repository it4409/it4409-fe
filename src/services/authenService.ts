import axios from "axios";
import { BASE_URL } from "../ultils/constant";
import axiosWithAuth from "../axiosInstance";

export const loginAPI = async (username: string, password: string) => {
  const res = await axios.post(`${BASE_URL}/auth/login`, {
    username,
    password,
  });
  return res;
};

export const registerAPI = async (
  name: string,
  email: string,
  username: string,
  mobilePhone: string,
  password: string,
  role: string
) => {
  const res = await axios.post(`${BASE_URL}/auth/signup`, {
    name,
    email,
    username,
    mobilePhone,
    password,
    role,
  });
  return res;
};

export const getUserInfo = async () => {
  const userInfo = (await axiosWithAuth.get("/user-info")).data;
  return userInfo;
};
