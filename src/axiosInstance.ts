import axios from "axios";
import { BASE_URL } from "./ultils/constant";
const axiosWithAuth = axios.create({
  baseURL: `${BASE_URL}`,
});

const getAccessToken = () => {
  const persistData = localStorage.getItem("persist:root");
  if (persistData) {
    const loginData = JSON.parse(JSON.parse(persistData).login);
    return loginData.token.accessToken;
  }
  return null;
};

axiosWithAuth.interceptors.request.use(
  async (config) => {
    const accessToken = await getAccessToken();
    config.headers.Authorization = "Bearer " + accessToken;
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default axiosWithAuth;
